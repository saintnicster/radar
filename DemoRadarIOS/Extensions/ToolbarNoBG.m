//
//  ToolbarNoBG.m
//  DemoRadar
//
//  Created by Nick Fajardo on 7/29/11.
//  Copyright 2011 Nick Fajardo. All rights reserved.
//
#import "ToolbarNoBG.h"

@implementation ToolbarNoBG

#pragma mark - 
#pragma mark UIView Override
- (id)initWithFrame:(CGRect)aRect {
	self = [super initWithFrame:aRect];
	if (self) {
		self.backgroundColor = [UIColor clearColor];
	}
	return self;
}
-(void) drawRect:(CGRect)rect {
    //Do nothing in here.  We don't want the background to show up.
}

@end