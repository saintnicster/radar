//
//  UIViewControllerExtension.h
//  RadarIOS
//
//  Created by Nick Fajardo on 11/15/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (MyAdditions)

-(void) openModalWindow:(UIViewController *)viewController Animated:(BOOL)isAnimated;
-(BOOL) closeModalWindowAnimated:(BOOL)isAnimated;

@end
