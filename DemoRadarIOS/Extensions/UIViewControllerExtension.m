//
//  UIViewControllerExtension.m
//  RadarIOS
//
//  Created by Nick Fajardo on 11/15/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "UIViewControllerExtension.h"

@implementation UIViewController (MyAdditions)

-(void)openModalWindow:(UIViewController *)viewController Animated:(BOOL)isAnimated {
    [self closeModalWindowAnimated:isAnimated];
    
    if ([self respondsToSelector:@selector(presentViewController:animated:completion:)]) {
        [self presentViewController:viewController animated:isAnimated completion:NULL];
    } else {
        [self presentModalViewController:viewController animated:isAnimated];
    }
    
}
-(BOOL) closeModalWindowAnimated:(BOOL)isAnimated {
    BOOL returnValue = NO;
    
    if ([self respondsToSelector:@selector(dismissViewControllerAnimated:completion:)]) {
        if (self.presentedViewController != nil) {
            [self dismissViewControllerAnimated:isAnimated completion:NULL];
            returnValue = YES;
        }
    } else {
        if (self.modalViewController != nil) {
            [self dismissModalViewControllerAnimated:isAnimated];
            returnValue = YES;
        }
    }
    
    return returnValue;
}

@end
