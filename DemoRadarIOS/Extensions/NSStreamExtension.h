//
//  NSStreamExtension.h
//  DemoRadar
//
//  Created by Nick Fajardo on 3/28/11.
//  Copyright 2011 Nick Fajardo. All rights reserved.
//  
//  Implementation/Work around found in Apple Technical Q&A QA1652
//
#import <Foundation/Foundation.h>


@interface NSStream (MyAdditions)

+ (void)getStreamsToHostNamed:(NSString *)hostName 
						 port:(NSInteger)port 
				  inputStream:(NSInputStream **)inputStreamPtr 
				 outputStream:(NSOutputStream **)outputStreamPtr;

@end