//
//  DemoEntity.m
//  DemoRadar
//
//  Created by Nick Fajardo on 3/28/11.
//  Copyright 2011 Nick Fajardo. All rights reserved.
//
#import "DemoEntity.h"
  
@implementation DemoEntity

#pragma mark -
#pragma mark Properties
@synthesize selected = _selected, objectID = _objectID, entityName = _entityName, entityType = _entityType, atZeroHP = _atZeroHP, coords = xyPosition, zCoord = zPosition;
-(NSString *) entityTypeName {
	NSString *typeName = nil;
	switch (self.entityType) {
		case EntityTypePlayer:
			typeName = @"Player";
			break;
		case EntityTypeTeammate:
			typeName = @"Teammate / Critter";
			break;
		case EntityTypeGlowie:
			typeName = @"Glowie";
			break;
		case EntityTypeGlowieWithHP:
			typeName = @"Glowie (with HP)";
			break;
		case EntityTypeGlowieInvis:
			typeName = @"Glowie (Invis)";
			break;
		case EntityTypeCaptive:
			typeName = @"NPC (Fear)";
			break;
		case EntityTypeHostage:
			typeName = @"Sacrifice";
			break;
		case EntityTypeQuantum:
			typeName = @"Nictus";
			break;
		case EntityTypeDoor:
			typeName = @"Door";
			break;
		case EntityTypePuddle:
			typeName = @"Puddle";
			break;
        case EntityTypeContactHasMish:
            typeName = @"Contact (mission available)";
			break;
        case EntityTypeContactInProg:
            typeName = @"Contact (mission in progress)";
			break;
        case EntityTypeContactMishComp:
            typeName = @"Contact (completed mission)";
			break;
		default:
			break;
	}
	return typeName;
}

#pragma mark -
#pragma mark Custom Methods
+(id) entityFromNSDictionary:(NSDictionary *)nsDict {
	DemoEntity *returnedEntity = nil;
	id dictObject = nil;
	
	dictObject = [nsDict objectForKey:@"objectID"];
	if ( dictObject != nil && [dictObject isKindOfClass:[NSString class]] ) {
		returnedEntity = [[DemoEntity alloc] initWithObjectID:(NSString *)dictObject];
	}
	if (returnedEntity == nil) {
		return nil;
	}
		
	dictObject =[nsDict objectForKey:@"entityName"];
	if ( dictObject != nil && [dictObject isKindOfClass:[NSString class]] ) {
		returnedEntity.entityName = dictObject;
        if (returnedEntity.entityName.length == 0) {
            returnedEntity.entityName = nil;
        }
	}
	
	dictObject =[nsDict objectForKey:@"entityTypeNbr"];
	if ( dictObject != nil && [dictObject isKindOfClass:[NSNumber class]] ) {
		returnedEntity.entityType = [dictObject integerValue];
	}
	
	dictObject =[nsDict objectForKey:@"entityIsDead"];
	if (dictObject != nil && [dictObject isKindOfClass:[NSNumber class]] ) {
		returnedEntity.atZeroHP = [dictObject boolValue];
	}
	
	float xPos = 0.0, yPos = 0.0, zPos = 0.0;	
	dictObject =[nsDict objectForKey:@"xPos"];
	if ( dictObject != nil && [dictObject isKindOfClass:[NSNumber class]] ) {
		xPos = [dictObject floatValue];
	}
	dictObject =[nsDict objectForKey:@"yPos"];
	if ( dictObject != nil && [dictObject isKindOfClass:[NSNumber class]] ) {
		yPos = [dictObject floatValue];
	}
	dictObject =[nsDict objectForKey:@"zPos"];
	if ( dictObject != nil && [dictObject isKindOfClass:[NSNumber class]] ) {
		zPos = [dictObject floatValue];
	}
	
	[returnedEntity setCoordinatesWithX:xPos y:yPos z:zPos];
	
	return returnedEntity;
}
-(id) initWithObjectID:(NSString *)objID {
    self = [super init];
	if (self) {
		_objectID = objID;
	}
	return self;
}

-(void) setCoordinatesWithX:(CGFloat)xValue y:(CGFloat)yValue z:(CGFloat)zValue {
	xyPosition = CGPointMake(xValue, yValue);
	zPosition = zValue;
}

#pragma mark -
#pragma mark NSObject Override
-(NSString *) description {	
	NSString *desc = 
		[NSString stringWithFormat:@"\nEntity\n\tName: %@ \n\tType: %@ (%u)\n\tPosition (x,y) = %@\n\tPosition (z): %@",
			self.entityName, self.entityTypeName, self.entityType, 
			NSStringFromCGPoint(xyPosition), [NSNumber numberWithFloat:zPosition]];
	return desc;
}

@end