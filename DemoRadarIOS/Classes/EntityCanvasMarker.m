//
//  EntityArrow.m
//  DemoRadar
//
//  Created by Nick Fajardo on 7/25/11.
//  Copyright 2011 Nick Fajardo. All rights reserved.
//
#import "EntityCanvasMarker.h"
@interface EntityCanvasMarker() {
    BOOL _shouldManuallyHandleTap;
    NSInteger _activeTapCount;
}
@property (assign) BOOL shouldManuallyHandleTap;
@property (assign) NSInteger activeTapCount;

-(void) handleTaps;
@end

@implementation EntityCanvasMarker
@synthesize shouldManuallyHandleTap = _shouldManuallyHandleTap, activeTapCount = _activeTapCount;
@synthesize delegate, entity = _entity, arrowStyle = _arrowStyle, arrowType = _arrowType;

#pragma mark -
#pragma mark Custom Stuff
-(void) handleTaps {
    BOOL shouldUnselect = NO;
    if (self.entity.selected) {
        shouldUnselect = YES;
    }
    
    self.entity.selected = !self.entity.selected;
    
    if (shouldUnselect) {
        [self.delegate didUnselectEntity:self.entity inView:self];
    } else {
        [self.delegate didSelectEntity:self.entity inView:self];
    }
}

#pragma mark -
#pragma mark UIView Override
-(void) drawRect:(CGRect)rect {
    UIColor *fillColor;
    
    switch (self.entity.entityType) {
        case EntityTypePlayer:
            fillColor = [UIColor greenColor];
            break;
        case EntityTypeTeammate:
            fillColor = [UIColor cyanColor];
            break;
        case EntityTypeDoor:
            fillColor = [UIColor blueColor];
            break;
        case EntityTypeHostage:
            fillColor = [UIColor redColor];
            break;
        case EntityTypeCaptive:
            fillColor = [UIColor redColor];
            break;
        case EntityTypeGlowie:
            fillColor = [UIColor yellowColor];
            break;
        case EntityTypeGlowieInvis:
            fillColor = [UIColor yellowColor];
            break;
        case EntityTypeGlowieWithHP:
            fillColor = [UIColor yellowColor];
            break;
        case EntityTypeQuantum:
            fillColor = [UIColor purpleColor];
            break;
        case EntityTypeContactHasMish:
            fillColor = [UIColor redColor];
            break;
        case EntityTypeContactInProg:
            fillColor = [UIColor redColor];
            break;
        case EntityTypeContactMishComp:
            fillColor = [UIColor redColor];
            break;
        default:
            fillColor = [UIColor whiteColor];
            break;
    }
    
    CGPoint boundsCenter = CGPointMake(self.bounds.size.width / 2, self.bounds.size.height / 2);
    
    Class bezier = NSClassFromString(@"UIBezierPath");
    if (bezier != nil) {
        [fillColor setFill];
        [[UIColor blackColor] setStroke];
        
        UIBezierPath *path = [bezier bezierPath];
        switch (self.arrowStyle) {
            case EntityMarkerStyleUp:
                [path moveToPoint:CGPointMake(boundsCenter.x, (boundsCenter.y - kEntityCanvasMarkerRadius))];
                [path addLineToPoint:CGPointMake((boundsCenter.x + kEntityCanvasMarkerRadius), (boundsCenter.y + kEntityCanvasMarkerRadius))];
                [path addLineToPoint:CGPointMake((boundsCenter.x - kEntityCanvasMarkerRadius), (boundsCenter.y + kEntityCanvasMarkerRadius))];
                [path addLineToPoint:CGPointMake((boundsCenter.x), (boundsCenter.y - kEntityCanvasMarkerRadius))];
                break;
            case EntityMarkerStyleDown:
                [path moveToPoint:CGPointMake(boundsCenter.x, (boundsCenter.y + kEntityCanvasMarkerRadius))];
                [path addLineToPoint:CGPointMake((boundsCenter.x + kEntityCanvasMarkerRadius), (boundsCenter.y - kEntityCanvasMarkerRadius))];
                [path addLineToPoint:CGPointMake((boundsCenter.x - kEntityCanvasMarkerRadius), (boundsCenter.y - kEntityCanvasMarkerRadius))];
                [path addLineToPoint:CGPointMake(boundsCenter.x, (boundsCenter.y + kEntityCanvasMarkerRadius))];
                break;
            case EntityMarkerStyleLevel:
                path = [bezier bezierPathWithOvalInRect:CGRectMake((boundsCenter.x - kEntityCanvasMarkerRadius), (boundsCenter.y - kEntityCanvasMarkerRadius), kEntityCanvasMarkerRadius*2, kEntityCanvasMarkerRadius*2)];
                break;
        }
        path.lineWidth = 2.0;
        [path fill];
        [path stroke];
        
        if (self.entity.selected) {
            [[UIColor whiteColor] setStroke];
            
            path = [UIBezierPath bezierPathWithRect:CGRectMake((boundsCenter.x - kEntityCanvasMarkerRadius), (boundsCenter.y - kEntityCanvasMarkerRadius), kEntityCanvasMarkerRadius*2, kEntityCanvasMarkerRadius*2)];
            path.lineWidth = 3;
            
            [path moveToPoint:CGPointMake(boundsCenter.x, CGRectGetMinY(self.bounds))];
            [path addLineToPoint:CGPointMake(boundsCenter.x, CGRectGetMaxY(self.bounds))];
            [path moveToPoint:CGPointMake(CGRectGetMinX(self.bounds), boundsCenter.y)];
            [path addLineToPoint:CGPointMake(CGRectGetMaxX(self.bounds), boundsCenter.y)];
            [path stroke];
        }
    } else {
        CGContextRef context = UIGraphicsGetCurrentContext();
        
        CGContextSetFillColorWithColor(context, fillColor.CGColor);
        CGContextSetStrokeColorWithColor(context, [UIColor blackColor].CGColor);
        CGContextSetLineWidth(context, 1.0);
        
        CGMutablePathRef path = CGPathCreateMutable();
        switch (self.arrowStyle) {
            case EntityMarkerStyleUp:
                CGPathMoveToPoint(path, NULL, boundsCenter.x, (boundsCenter.y - kEntityCanvasMarkerRadius));
                CGPathAddLineToPoint(path, NULL, (boundsCenter.x + kEntityCanvasMarkerRadius), (boundsCenter.y + kEntityCanvasMarkerRadius));
                CGPathAddLineToPoint(path, NULL, (boundsCenter.x - kEntityCanvasMarkerRadius), (boundsCenter.y + kEntityCanvasMarkerRadius));
                CGPathAddLineToPoint(path, NULL, (boundsCenter.x), (boundsCenter.y - kEntityCanvasMarkerRadius));
                break;
            case EntityMarkerStyleDown:
                CGPathMoveToPoint(path, NULL, (boundsCenter.x), (boundsCenter.y + kEntityCanvasMarkerRadius));
                CGPathAddLineToPoint(path, NULL, (boundsCenter.x + kEntityCanvasMarkerRadius), (boundsCenter.y - kEntityCanvasMarkerRadius));
                CGPathAddLineToPoint(path, NULL, (boundsCenter.x - kEntityCanvasMarkerRadius), (boundsCenter.y - kEntityCanvasMarkerRadius));
                CGPathAddLineToPoint(path, NULL, (boundsCenter.x), (boundsCenter.y + kEntityCanvasMarkerRadius));
                break;
            case EntityMarkerStyleLevel:
                CGPathAddEllipseInRect(path, NULL, CGRectMake((boundsCenter.x - kEntityCanvasMarkerRadius), 
                                                              (boundsCenter.y - kEntityCanvasMarkerRadius), 
                                                              kEntityCanvasMarkerRadius*2, 
                                                              kEntityCanvasMarkerRadius*2));
                break;
        }
        
        if (! CGPathIsEmpty(path) ) {
            CGPathCloseSubpath(path);
            
            CGContextAddPath(context, path);
            CGContextFillPath(context);
            CGContextAddPath(context, path);
            CGContextStrokePath(context);
        }
        CGPathRelease(path);
        
        if (self.entity.selected) {
            CGContextSetLineWidth(context, 3);
            CGContextSetStrokeColorWithColor(context, [UIColor blackColor].CGColor);

            CGMutablePathRef reticlePath = CGPathCreateMutable();
            CGPathAddRect(reticlePath, NULL, CGRectMake((boundsCenter.x - kEntityCanvasMarkerRadius), (boundsCenter.y - kEntityCanvasMarkerRadius), kEntityCanvasMarkerRadius*2, kEntityCanvasMarkerRadius*2));
            CGPathMoveToPoint(reticlePath, NULL, boundsCenter.x, CGRectGetMinY(self.bounds));
            CGPathAddLineToPoint(reticlePath, NULL, boundsCenter.x, CGRectGetMaxY(self.bounds));
            CGPathMoveToPoint(reticlePath, NULL, CGRectGetMinX(self.bounds), boundsCenter.y);
            CGPathAddLineToPoint(reticlePath, NULL, CGRectGetMaxX(self.bounds), boundsCenter.y);
            
            CGPathCloseSubpath(reticlePath);
            CGContextAddPath(context, reticlePath);
            
            CGContextStrokePath(context);
            CGPathRelease(reticlePath);
        }
    }
}

#pragma mark -
#pragma mark NSResponder Override
-(void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    if (self.shouldManuallyHandleTap) {
        self.activeTapCount = [[touches anyObject] tapCount];
    }
}
-(void) touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    if (self.shouldManuallyHandleTap) {
        self.activeTapCount = 0;
    }
}
-(void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    if (self.shouldManuallyHandleTap) {
        if (self.activeTapCount == 1) {
            [self handleTaps];
        }
        self.activeTapCount = 0;
    }
}
-(void) touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    if (self.shouldManuallyHandleTap) {
        self.activeTapCount = 0;
    }
}

#pragma mark -
#pragma mark NSObject Override
-(id) init {
    self = [super init];
    if (self) {
        self.opaque = NO;
		
        _arrowStyle = EntityMarkerStyleLevel;
        self.shouldManuallyHandleTap = NO;
        
        
        id tap = nil;
        Class tapRecognizer = NSClassFromString(@"UITapGestureRecognizer");
        if (tapRecognizer != nil) {
            tap = [[tapRecognizer alloc] initWithTarget:self action:@selector(handleTaps)];
        }
        
        if (tap != nil && [tap respondsToSelector:@selector(locationInView:)]) {
            ((UITapGestureRecognizer *)tap).numberOfTapsRequired = 1;
            
            [self addGestureRecognizer:tap];
        } else {
            self.shouldManuallyHandleTap = YES;
        }
        
        self.frame = CGRectMake(0, 0, kEntityCanvasMarkerFrameSide, kEntityCanvasMarkerFrameSide);
    }
    return self;
}

@end 