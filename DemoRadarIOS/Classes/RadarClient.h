//
//  RadarServer.h
//  DemoRadar
//
//  Created by Nick Fajardo on 7/5/11.
//  Copyright 2011 Nick Fajardo. All rights reserved.
//
#import <Foundation/Foundation.h>
#import "NSStreamExtension.h"

@protocol RadarClientDelegate
-(void) didReceiveEntityList:(NSArray *)listOfEntities;
-(void) errorConnectingToServerWithMessage:(NSString *)errorMsg;
-(void) errorReadngFileWithMessage:(NSString *)errorMsg;
@end

@interface RadarClient : NSObject <NSStreamDelegate> {
	id <RadarClientDelegate> __unsafe_unretained _delegate;
}
@property (unsafe_unretained) id<RadarClientDelegate> delegate;
@property (readonly) BOOL hostIsSet;

-(void) setHostName:(NSString *)hostname andPort:(NSInteger)portNumber;
-(void) getDemoFromHost;

@end