//
//  EntityArrow.h
//  DemoRadar
//
//  Created by Nick Fajardo on 7/25/11.
//  Copyright 2011 Nick Fajardo. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "DemoEntity.h"

#define kEntityCanvasMarkerFrameSide    60
#define kEntityCanvasMarkerRadius       20
#define kEntityCanvasMarkerEdgeBuffer    4
typedef enum {
	EntityMarkerStyleLevel,
	EntityMarkerStyleUp,
	EntityMarkerStyleDown,
} EntityMarkerStyle;

@protocol EntityCanvasMarkerDelegate
-(void) didSelectEntity:(DemoEntity *)entity inView:(UIView *)view ;
-(void) didUnselectEntity:(DemoEntity *)entity inView:(UIView *)view ;
@end


@interface EntityCanvasMarker : UIView {
    id<EntityCanvasMarkerDelegate> __unsafe_unretained delegate;
    
    int _arrowType;
	EntityMarkerStyle _arrowStyle;
	DemoEntity *_entity;
}
@property (unsafe_unretained) id<EntityCanvasMarkerDelegate> delegate;
@property (assign) int arrowType;
@property (assign) EntityMarkerStyle arrowStyle;
@property (strong) DemoEntity *entity;

@end