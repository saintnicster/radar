//
//  DemoEntity.h
//  DemoRadar
//
//  Created by Nick Fajardo on 3/28/11.
//  Copyright 2011 Nick Fajardo. All rights reserved.
//
#import <Foundation/Foundation.h>
// typedef enum {
	// EntityTypeNone = NSIntegerMax,
	// EntityTypePlayer,
	// EntityTypeTeammate,
	// EntityTypeGlowie,
	// EntityTypeGlowieWithHP,
	// EntityTypeGlowieInvis,
	// EntityTypeCaptive,
	// EntityTypeHostage,
	// EntityTypeQuantum,
	// EntityTypeDoor,
	// EntityTypePuddle,
// } EntityType;

#define EntityTypePlayer		  0
#define EntityTypeTeammate		  1
#define EntityTypeGlowie		  2
#define EntityTypeGlowieWithHP	  3
#define EntityTypeGlowieInvis	  4
#define EntityTypeContactHasMish  5
#define EntityTypeContactInProg   6
#define EntityTypeContactMishComp 7
#define EntityTypeCaptive		  8
#define EntityTypeHostage		  9
#define EntityTypeQuantum		  10
#define EntityTypeDoor			  11
#define EntityTypePuddle		  12
#define EntityTypeNone			  NSIntegerMax


@interface DemoEntity : NSObject {
	NSString *__unsafe_unretained _objectID;
	NSString *_entityName;
	NSInteger _entityType;

    BOOL _selected;
	BOOL _atZeroHP;
	CGPoint xyPosition;
	CGFloat zPosition;
}
@property (readonly) NSString *objectID;
@property (strong)   NSString *entityName;
@property (assign)   NSInteger entityType;
@property (unsafe_unretained, readonly) NSString *entityTypeName;
@property (assign)   BOOL atZeroHP;
@property (readonly) CGPoint coords;
@property (readonly) CGFloat zCoord;
@property (assign) BOOL selected;

+(id) entityFromNSDictionary:(NSDictionary *)nsDict;
-(id) initWithObjectID:(NSString *)objID;

-(void) setCoordinatesWithX:(CGFloat)xValue y:(CGFloat)yValue z:(CGFloat)zValue;

@end