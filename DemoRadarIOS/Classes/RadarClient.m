//
//  RadarServer.m
//  DemoRadar
//
//  Created by Nick Fajardo on 7/5/11.
//  Copyright 2011 Nick Fajardo. All rights reserved.
//
#import "RadarClient.h"

@interface RadarClient() {
	BOOL isFirstPacket, isConnected, retriedLoad;
	NSString *_host, *_fileName;
	NSInteger _port, bufferSize;
	NSInputStream *_inputStream;
	NSMutableData *incomingData;
}

@property (strong) NSString *fileName;
@property (strong) NSString *host;
@property (assign) NSInteger bufferSize;
@property (assign) NSInteger port;
@property (nonatomic, strong) NSInputStream *inputStream;
@property (strong) NSMutableData *incomingData;

-(void) closeReceiver;
-(BOOL) processPacketFromServer:(NSInputStream *)inStream;
-(void) interpretByteStream;

@end


@implementation RadarClient
#pragma mark -
#pragma mark Properties
@synthesize delegate = _delegate;
@synthesize inputStream = _inputStream, host = _host, port = _port, fileName = _fileName, bufferSize = _bufferSize, incomingData = _incomingData;
-(BOOL) hostIsSet {
	return (self.host != nil && self.port > 0) ? YES : NO;
}

#pragma mark -
#pragma mark Custom Stuff
-(void) setHostName:(NSString *)hostname andPort:(NSInteger)portNumber {
	self.host = hostname;
	self.port = portNumber;
}
-(void) getDemoFromHost {
	isFirstPacket = YES;
	isConnected = NO;
	self.fileName = nil;
	self.incomingData = nil;
	
	NSInputStream *tmpInput = nil;
	[NSStream getStreamsToHostNamed: self.host
							   port: self.port
						inputStream: &tmpInput
					   outputStream: NULL];
	self.inputStream = tmpInput;
	
	self.inputStream.delegate = self; 
	[self.inputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
	[self.inputStream open];
}
-(void) closeReceiver {
	if (self.inputStream != nil) {
		self.inputStream.delegate = nil;
			
		[self.inputStream removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
		[self.inputStream close];
		
		self.inputStream = nil;
	}
}
-(BOOL) processPacketFromServer:(NSInputStream *)inStream {
	NSData	*data = nil;
	BOOL closeStream = NO;
	NSInteger bytesRead = 0, bufferLength = 0;
	
	
	if (isFirstPacket) {
		bufferLength = 50;
	} else {
		bufferLength = self.bufferSize;
	}
	
	uint8_t buffer[bufferLength];
	
	if ([inStream hasBytesAvailable]) {
		bytesRead = [self.inputStream read:buffer maxLength:sizeof(buffer)];
	}
	
	if (bytesRead == -1) {
		//NSLog(@"Network read error");
		closeStream = YES;
	} else if (bytesRead == 0) {
		//NSLog(@"Buffer is Empty");
		closeStream = YES;
	} else {
		//NSLog(@"Bytes Read: %u", bytesRead);
		//NSLog(@"Buffer Size: %u", bufferLength);
		
		data = [NSData dataWithBytes:buffer length:bytesRead];
		if (isFirstPacket) {
            NSString *fileInfo = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            //NSLog(fileInfo, nil);
            //NSLog(@"File Info Line - %@", fileInfo);
			
			NSArray *chunks = [fileInfo componentsSeparatedByString: @":"];
			
			if (chunks.count == 2) {
				self.fileName = [chunks objectAtIndex:0];
				self.bufferSize = [[chunks objectAtIndex:1] intValue];
			} else {
				//NSLog(@"Invalid File Line -- %@", fileInfo);
				self.fileName = nil;
				self.bufferSize = 40000;
			}
			isFirstPacket = NO;
			self.incomingData = [NSMutableData data];
            
		} else {
			[self.incomingData appendData:data];
		}
	}
	
	return closeStream;
}
-(void) interpretByteStream {
	if ( [self.fileName rangeOfString:@".plist"].location != NSNotFound ) {
		NSError *error = nil;
		NSArray *entityPList = nil;
		
		entityPList = [NSPropertyListSerialization propertyListWithData:self.incomingData
																options:NSPropertyListImmutable
																 format:NULL 
																  error:&error];
		if (error != nil) {
			//NSLog(@"Bytestream Error - %@", error);
			if (error.code == 3840 || retriedLoad == YES) {
                [self getDemoFromHost];
                NSLog([NSString stringWithUTF8String:[self.incomingData bytes]], nil);
            } else {
                retriedLoad = YES;
                [self.delegate errorReadngFileWithMessage:error.localizedDescription];
            }
		} else {
			[self.delegate didReceiveEntityList:entityPList];
            retriedLoad = NO;
		}
	} else {
        [self.delegate errorReadngFileWithMessage:@"File wasn't in the correct format.  Please check for program and App updates"];
    }
}

#pragma mark -
#pragma mark NSStreamDelegate implementation
-(void) stream:(NSStream *)aStream handleEvent:(NSStreamEvent)eventCode {
    BOOL closeStream = NO, errorDidHappen = NO;
    
    switch (eventCode) {
        case NSStreamEventNone:
            //NSLog( @"No Stream Event?");
            break;
        case NSStreamEventOpenCompleted:
            //NSLog( @"Event Opened");
            isConnected = YES;
            break;
        case NSStreamEventHasBytesAvailable:
            //NSLog( @"Received Bytes");
            
            closeStream = [self processPacketFromServer:(NSInputStream *)aStream];
            
            break;
        case NSStreamEventHasSpaceAvailable:
            //NSLog( @"Space Available - SHOULD NOT SEE (Event Handler is only used on outgoing streams)");
            break;
        case NSStreamEventErrorOccurred:
            //NSLog( @"Error Occured");
            if (self.delegate != nil) {
                if (isConnected) {
                    [self.delegate errorReadngFileWithMessage:aStream.streamError.localizedDescription];
                } else {
                    [self.delegate errorConnectingToServerWithMessage:aStream.streamError.localizedDescription];
                }
            }
            errorDidHappen = YES;
            closeStream = YES;
            break;
        case NSStreamEventEndEncountered:
            //NSLog( @"Stream Ended");
            closeStream	 = YES;
            break;
            
        default:
            //NSLog(@"Y U NO WERK");
            break;
    }
    
    if (closeStream) {
        [self closeReceiver];
        
        if ( self.incomingData != nil ) {
            [self interpretByteStream];
        } else if (!errorDidHappen) {
            [self.delegate errorReadngFileWithMessage:@"No map data received from server.  Please try reloading"];
        }
    }
}

@end