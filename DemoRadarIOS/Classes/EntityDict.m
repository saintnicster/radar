//
//  EntityDict.m
//  DemoRadar
//
//  Created by Nick Fajardo on 7/21/11.
//  Copyright 2011 Nick Fajardo. All rights reserved.
//
#import "EntityDict.h"
@interface EntityDict(){
    NSMutableArray *_enumerator;
}
@property (strong) NSMutableArray *enumerator;
@end

@implementation EntityDict
#pragma mark - 
#pragma mark Property Implementations
@synthesize enumerator = _enumerator;
-(DemoEntity *) playerEntity {
    return [self.dict objectForKey:_playerEntity];
}
-(NSMutableDictionary *) dict {
    if (_dict == nil) { 
        _dict = [[NSMutableDictionary alloc] init]; 
        maxX = NSIntegerMin;
        maxY = NSIntegerMin;
        minX = NSIntegerMax;
        minY = NSIntegerMax;
    }
    return _dict;
}
-(NSMutableArray *) keys {
    if (_keys == nil) _keys = [[NSMutableArray alloc] init];
    return _keys;
}
-(CGPoint) maxCoords {
    return CGPointMake(maxX, maxY);
}
-(CGPoint) minCoords {
    return CGPointMake(minX, minY);
}

#pragma mark - 
#pragma mark Custom Stuff
-(void) updateMinMaxWithCoord:(CGPoint)coords {
    if ( coords.x > maxX ) {
        maxX = coords.x; 
    }
    if ( coords.x < minX ) {
        minX = coords.x; 
    }
    if ( coords.y > maxY ) {
        maxY = coords.y; 
    }
    if ( coords.y < minY ) {
        minY = coords.y; 
    }
}
-(void) addEntity:(DemoEntity *)entity {
    NSString *key = entity.objectID;
    [self.dict setValue:entity forKey:key];
    [self.keys addObject:key];
    
    [self updateMinMaxWithCoord:entity.coords];
    
    if (entity.entityType == EntityTypePlayer) {
        _playerEntity = entity.objectID;
    }
}
-(DemoEntity *) entityFromIndex:(NSIndexPath *)indexPath {
    return [self.dict objectForKey:[self.keys objectAtIndex:indexPath.row]];
}

#pragma mark - 
#pragma mark NSEnumerator/NSFastEnumeration
-(NSArray *) allObjects {
    if (self.enumerator == nil) {
        self.enumerator = [NSMutableArray array];
        
        for (NSString *key in self.keys) {
            [self.enumerator addObject:[self.dict objectForKey:key]];
        }
    }
    return self.enumerator;
}
-(id) nextObject {
    return [[self.enumerator objectEnumerator] nextObject];
}
-(NSUInteger) countByEnumeratingWithState:(NSFastEnumerationState *)state objects:(id __unsafe_unretained [])stackbuf count:(NSUInteger)len {
    if (self.enumerator == nil) { 
		[self allObjects];
        self.enumerator = (NSMutableArray *) [self.enumerator reverseObjectEnumerator];
	}
    return [self.enumerator countByEnumeratingWithState:state objects:stackbuf count:len];
}

#pragma mark - 
#pragma mark NSObject Override
@end