//
//  EntityDict.h
//  DemoRadar
//
//  Created by Nick Fajardo on 7/21/11.
//  Copyright 2011 Nick Fajardo. All rights reserved.
//
#import <Foundation/Foundation.h>
#import "DemoEntity.h"

@interface EntityDict : NSEnumerator <NSFastEnumeration> {
    NSMutableDictionary *_dict;
    NSMutableArray *_keys;
    NSString *_playerEntity;
    float minX, minY, maxX, maxY;
}

@property (readonly) CGPoint maxCoords;
@property (readonly) CGPoint minCoords;
@property (unsafe_unretained, readonly) DemoEntity *playerEntity;
@property (unsafe_unretained, readonly) NSMutableDictionary *dict;
@property (unsafe_unretained, readonly) NSMutableArray *keys;

-(void) updateMinMaxWithCoord:(CGPoint)coords;
-(void) addEntity:(DemoEntity *)entity;
-(DemoEntity *) entityFromIndex:(NSIndexPath *)index;

@end