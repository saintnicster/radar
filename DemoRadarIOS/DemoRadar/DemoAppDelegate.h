//
//  DemoAppDelegate.h
//  DemoRadar
//
//  Created by Nick Fajardo on 11/21/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MapViewController.h"


@interface DemoAppDelegate : UIResponder <UIApplicationDelegate> {
    MapViewController *_mapView;
    UINavigationController *_navCon;
}

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UINavigationController *navCon;
@property (strong, nonatomic) MapViewController *mapView;

@end
