//
//  DemoAppDelegate.m
//  DemoRadar
//
//  Created by Nick Fajardo on 11/21/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "DemoAppDelegate.h"

@implementation DemoAppDelegate

@synthesize window = _window, mapView = _mapView, navCon = _navCon;


#pragma mark NSObject

#pragma mark UIApplicationDelegate
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.mapView = [[MapViewController alloc] init];
    self.navCon = [[UINavigationController alloc] initWithRootViewController:self.mapView];
    
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window addSubview:self.navCon.view];
    
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    [self.mapView appEnteringBackground];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    [self.mapView appEnteringForeground];
}

@end
