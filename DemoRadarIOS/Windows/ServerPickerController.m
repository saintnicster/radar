//
//  ServerPickerController.m
//  DemoRadar
//
//  Created by Nick Fajardo on 3/26/11.
//  Copyright 2011 Nick Fajardo. All rights reserved.
//
//  Idea for checking WiFi connection inspired from
//  http://stackoverflow.com/questions/1083701/how-to-check-for-an-active-internet-connection-on-iphone-sdk/

#import "ServerPickerController.h"
@interface ServerPickerController() {
    UIAlertView *reachAlert;
}
@end

@implementation ServerPickerController

#pragma mark -
#pragma mark Properties
@synthesize delegate, cancelButton, tableView;
@synthesize foundServices = _foundServices;
-(NSMutableArray *) foundServices {
	if (_foundServices == nil) _foundServices = [[NSMutableArray alloc] init];
	return _foundServices;
}

#pragma mark -
#pragma mark IBActions
-(IBAction) cancelAction {
	[delegate didCancelServerSelection];
}
-(IBAction) connectToServer {
	[delegate didCancelServerSelection];
}

#pragma mark - 
#pragma mark Custom
-(void) checkNetworkStatus:(Reachability*)reach{
    // called after network status changes
    
    BOOL wifi = [localWifiReachable isReachableViaWiFi];
    BOOL wwan = [localWifiReachable isReachableViaWWAN];
    [localWifiReachable isConnectionRequired];
    NSString *message = nil;
    
    if (!wifi) {
        if (wwan) {
            message = [NSString stringWithString:@"Browsing for servers only works while connected to your local network over wifi."];
        } else {
            message = [NSString stringWithString:@"Not connected to WiFi.\n\nPlease connect to browse for DemoRadar server software."];
        }
        if (reachAlert != nil) {
            if ([reachAlert.message isEqualToString:message]) {
                return;
            }
        }
        reachAlert = [[UIAlertView alloc] initWithTitle:@"Message"
                                                 message:message
                                                delegate:self
                                       cancelButtonTitle:@"OK"
                                      otherButtonTitles:nil];
        [reachAlert show];
    }
}

#pragma mark -
#pragma mark UIAlertViewDelegate
-(void) alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    reachAlert = nil;
}

#pragma mark -
#pragma mark UIViewConroller
-(id) initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.tabBarItem.title = @"Scanner";
    }
    return self;
}
-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self 
                                             selector:@selector(checkNetworkStatus:) 
                                                 name:kReachabilityChangedNotification
                                               object:nil];
    
    localWifiReachable = [Reachability reachabilityForInternetConnection];
    [localWifiReachable startNotifier];
    [self checkNetworkStatus:localWifiReachable];
    
	_browser = [[NSNetServiceBrowser alloc] init];
	_browser.delegate = self;
	[_browser searchForServicesOfType:@"_demoradar._tcp" inDomain:@""];
}
-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self checkNetworkStatus:localWifiReachable];
    
	[self.tableView flashScrollIndicators];
}
-(void) viewWillDisappear:(BOOL)animated {
	[_browser stop];
    _browser = nil;
    [_foundServices removeAllObjects];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [super viewWillDisappear:animated];
}
-(BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return YES;
}

#pragma mark -
#pragma mark NSNetServiceDelegate implementation
-(void) netServiceDidResolveAddress:(NSNetService *)sender {
	//NSLog(@"Hostname: %@, Address %@, Port %d", sender.hostName, [[sender addresses] objectAtIndex:0], sender.port);
    
	[delegate didSelectHostname:sender.hostName andPort:sender.port];
}
-(void) netService:(NSNetService *)sender didNotResolve:(NSDictionary *)errorDict {
	//for (id error in errorDict) {
	//	NSLog(@"%@", error);
	//}
	
	[tableView reloadData];
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message"
                                                     message:@"Unable to determine address. Please select another machine"
                                                    delegate:nil 
                                           cancelButtonTitle:@"OK" 
                                           otherButtonTitles:nil];
	[alert show];
}

#pragma mark -
#pragma mark NSNetServiceBrowserDelegate implementation
-(void) netServiceBrowser:(NSNetServiceBrowser *)netServiceBrowser
		   didFindService:(NSNetService *)netService
			   moreComing:(BOOL)moreServicesComing  {
	[self.foundServices addObject:netService];
	
	if (moreServicesComing == NO) {
		[tableView reloadData];
	}
}
-(void) netServiceBrowser:(NSNetServiceBrowser *)netServiceBrowser
		 didRemoveService:(NSNetService *)netService
			   moreComing:(BOOL)moreServicesComing {
	[self.foundServices removeObject:netService];
    
	if (moreServicesComing == NO) {
		[tableView reloadData];
	}
}
-(void) netServiceBrowserDidStopSearch:(NSNetServiceBrowser *)aNetServiceBrowser {
	[self.foundServices removeAllObjects];
    NSLog(@"Stopping Search");
	
	[tableView reloadData];
}
-(void) netServiceBrowser:(NSNetServiceBrowser *)aNetServiceBrowser didNotSearch:(NSDictionary *)errorDict {
    NSLog(@"errors - %@", errorDict);
}

#pragma mark -
#pragma mark UITableViewDataSource implementation
-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.foundServices.count;
}
-(UITableViewCell *) tableView:(UITableView *)localTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [localTableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
		cell.textLabel.text = @"Blank Cell";
    }
	
	if (self.foundServices.count > 0) {
		NSNetService *netService = [self.foundServices objectAtIndex:indexPath.row];
		
		if (netService != nil) {
            cell.textLabel.text = netService.name;
        }
	}
	
    return cell;
}

#pragma mark -
#pragma mark UITableViewDelegate implementation
-(NSString *) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
	return @"Servers Found";
}
-(void) tableView:(UITableView *)localTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	if (self.foundServices.count > 0) {
		NSNetService *service = [self.foundServices objectAtIndex:indexPath.row];
		if (service != nil) {
			service.delegate = self;
			[service resolveWithTimeout:5];
		}
	}
}
@end