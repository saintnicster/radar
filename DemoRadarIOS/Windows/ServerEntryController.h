//
//  ServerEntryController.h
//  DemoRadar
//
//  Created by Nick Fajardo on 9/24/11.
//  Copyright 2011 Nick Fajardo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServerPickerDelegate.h"

@interface ServerEntryController : UIViewController {
    id<ServerPickerDelegate> __unsafe_unretained delegate;
    
	UITextField *hostAddress;
	UITextField *port;
}
@property (nonatomic, strong) IBOutlet UITextField *hostAddress;
@property (nonatomic, strong) IBOutlet UITextField *port;

@property (unsafe_unretained) id<ServerPickerDelegate> delegate;

-(IBAction) connectToServer;
-(IBAction) cancelSelection;

@end
