//
//  MVRightBBIWrapper.h
//  DemoRadar
//
//  Created by Nick Fajardo on 7/29/11.
//  Copyright 2011 Nick Fajardo. All rights reserved.
//	
#import <UIKit/UIKit.h>
#import "ToolbarNoBG.h"

typedef enum {
	MVRBBIWButtonTypeAction,
	MVRBBIWButtonTypeParse,
} MVRBBIWButtonType;

@protocol MVRBBIWDelegate
-(void) userDidSelectButton:(MVRBBIWButtonType)buttonType;
@end

@interface MVRightBBIWrapper : NSObject {
	UIBarButtonItem *_barButtonItemView;
	id <MVRBBIWDelegate> __unsafe_unretained delegate;
    UIBarButtonItem *_actionButton;
    UIBarButtonItem *_parseButton;
}

@property (strong) UIBarButtonItem *barButtonItemView;
@property (unsafe_unretained) id <MVRBBIWDelegate> delegate;
@property (strong) UIBarButtonItem *actionButton;
@property (strong) UIBarButtonItem *parseButton;

@end