//
//  MapCanvas.m
//  DemoRadar
//
//  Created by Nick Fajardo on 7/23/11.
//  Copyright 2011 Nick Fajardo. All rights reserved.
//
#import "MapCanvas.h"
#define ContentBuffer 50
#define CANVAS_BUFFER_BIG 100
#define CANVAS_BUFFER_SMALL 50

@interface MapCanvas() {
    int _xBuffer;
    int _yBuffer;
	
	CGPoint _xyScaler;
    CGSize _unzoomedContentSize;
    NSMutableDictionary *_arrows;
}
@property (assign) CGPoint xyScaler;
@property (assign) int xBuffer;
@property (assign) int yBuffer;
@property (strong) NSMutableDictionary *arrows;

-(void) setUnzoomedContentSize:(CGSize)newUnzoomedContentSize;
-(CGPoint) convertEntityPointToMainView:(CGPoint)entityPoint;
@end

@implementation MapCanvas
@synthesize xyScaler = _xyScaler, xBuffer = _xBuffer, yBuffer = _yBuffer, arrows = _arrows;
@synthesize containerView = _containerView, dict = _dict, unzoomedContentSize=_unzoomedContentSize;

#pragma mark -
#pragma mark Custom Stuff
-(void) setUnzoomedContentSize:(CGSize)newUnzoomedContentSize {
    _unzoomedContentSize = CGSizeApplyAffineTransform(newUnzoomedContentSize, CGAffineTransformMakeScale(1.5f, 1.5f));
    if (self.unzoomedContentSize.width > self.unzoomedContentSize.height) {
        self.xBuffer = CANVAS_BUFFER_SMALL;
        self.yBuffer = CANVAS_BUFFER_BIG;
    } else {
        self.xBuffer = CANVAS_BUFFER_BIG;
        self.yBuffer = CANVAS_BUFFER_SMALL;
    }
    NSLog(@"Content Size (No Zoom) = %@", NSStringFromCGSize(_unzoomedContentSize));
}
-(void) resetContentSize {
    if (CGSizeEqualToSize(self.unzoomedContentSize, CGSizeZero)) {
        self.unzoomedContentSize = [UIScreen mainScreen].bounds.size;
    }
    
    CGFloat scaleFactor = [self zoomScale];
    if ([[[self window] screen] respondsToSelector:@selector(scale)]) {
        scaleFactor *= [[[self window] screen] scale];
    }

    
    CGSize tmpContentSize = self.unzoomedContentSize;
    tmpContentSize.height *= scaleFactor;
    tmpContentSize.width *= scaleFactor;
    self.contentSize = tmpContentSize;
    self.containerView.frame = CGRectMake(0, 0, self.contentSize.width, self.contentSize.height);
        
    NSLog(@"Content Size = %@", NSStringFromCGSize(self.contentSize));
}
-(void) clearMap {
    for (id subview in self.containerView.subviews) {
        [(UIView *)subview removeFromSuperview];
    }
}
-(void) displayEntities {
    [self setZoomScale:.75 animated:NO];
    
    self.xyScaler = CGPointZero;
    [self resetContentSize];
    
	CGFloat midPoint = self.dict.playerEntity.zCoord;
	
	EntityCanvasMarker *arrow = nil;
    EntityCanvasMarker *playerArrow = nil;
	
    self.arrows = [NSMutableDictionary dictionary];
    
    for (DemoEntity *item in self.dict) {
		arrow = [[EntityCanvasMarker alloc] init];
		arrow.entity = item;
        arrow.delegate = self;
		
		if ( item.zCoord > (midPoint + 3)) {
			arrow.arrowStyle = EntityMarkerStyleUp;
		} else if (item.zCoord < (midPoint - 3) ) {
			arrow.arrowStyle = EntityMarkerStyleDown;
		} else {
			arrow.arrowStyle = EntityMarkerStyleLevel;
		}
		CGPoint adjPoint = [self convertEntityPointToMainView:item.coords];
        arrow.center = adjPoint;
        
        [self.arrows setValue:arrow forKey:item.objectID];
        
        if (item.entityType == EntityTypePlayer) {
            playerArrow = arrow;
        } else {
            [self.containerView addSubview:arrow];
            
        }
	}
    
    if (playerArrow != nil) {
        [self.containerView addSubview:playerArrow];
    }
    
    if (self.containerView.subviews != nil) {
        [self flashScrollIndicators];
    }
}
-(CGPoint) convertEntityPointToMainView:(CGPoint)entityPoint {
	CGPoint retValue = entityPoint;
	
    CGPoint minPoint = self.dict.minCoords, 
    maxPoint = self.dict.maxCoords;
    
	if (CGPointEqualToPoint(self.xyScaler, CGPointZero)) {
        //NSLog(@"Min - %@", NSStringFromCGPoint(minPoint));
        //NSLog(@"Max - %@", NSStringFromCGPoint(maxPoint));
                
        CGSize demo = CGSizeZero;
		demo.width  = ((int)(maxPoint.x - minPoint.x));
        demo.height = ((int)(maxPoint.y - minPoint.y));
        
		self.xyScaler = CGPointMake( ( (self.contentSize.width) / demo.width ), ( (self.contentSize.height) / demo.height ) );
        
        //NSLog(@"Bounds Size - %@", NSStringFromCGSize(self.contentSize));
        //NSLog(@"Demo Size = %@", NSStringFromCGSize(demo));
        //NSLog(@"Scalers %@", NSStringFromCGPoint(self.xyScaler));
    }
	
    retValue.x = maxPoint.x - retValue.x;
    retValue.y = retValue.y - (minPoint.y);
    
    retValue.x = (int)(retValue.x * self.xyScaler.x) + self.xBuffer;
	retValue.y = (int)(retValue.y * self.xyScaler.y) + self.yBuffer;
    
    return retValue;
}
-(void) setEntityForRedisplay:(NSString *)entityID {
    EntityCanvasMarker *tmpArrow = [self.arrows objectForKey:entityID];
    if (tmpArrow != nil) {
        [tmpArrow setNeedsDisplay];
        [self.containerView bringSubviewToFront:tmpArrow];
    }
}

#pragma mark -
#pragma mark UIScrollViewDelegate implementation
- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return self.containerView;
}
- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(float)scale {
    if ([[[self window] screen] respondsToSelector:@selector(scale)]) {
        scale *= [[[scrollView window] screen] scale];
    }
	
    for (UIView *sub in self.containerView.subviews) {
        sub.contentScaleFactor = scale;
    }
    
    [self resetContentSize];
}

#pragma mark -
#pragma mark EntityArrowDelegate implementation
-(void) didSelectEntity:(DemoEntity *)entity inView:(UIView *)view {
    //NSLog(@"Selected Entity - %@", entity);
    
    [self.containerView bringSubviewToFront:view];
    [view setNeedsDisplay];
}
-(void) didUnselectEntity:(DemoEntity *)entity inView:(UIView *)view {
    //NSLog(@"Unselected Entity - %@", entity);
    
    [view setNeedsDisplay];
}

#pragma mark -
#pragma mark UIScrollView<NSCoding> Override
- (id)initWithCoder:(NSCoder *)decoder {
    self = [super initWithCoder:decoder];
    if (self) {
		self.containerView = [[UIView alloc] init];
		self.containerView.frame = CGRectMake(0, 0, self.contentSize.width+200, self.contentSize.height+200);
		[self addSubview:self.containerView];
        
        self.indicatorStyle = UIScrollViewIndicatorStyleWhite;
        
		self.delegate = self;
		self.minimumZoomScale = 0.2;
		self.maximumZoomScale = 5.0;
        self.zoomScale = 1;
    }
    return self;
}

#pragma mark -
#pragma mark NSObject Override
-(void) dealloc {
    [self clearMap];
    
}
@end