//
//  ServerTabBarController.m
//  DemoRadar
//
//  Created by Nick Fajardo on 9/24/11.
//  Copyright 2011 Nick Fajardo. All rights reserved.
//

#import "ServerTabBarController.h"

@interface ServerTabBarController()
-(void) initTBC;
-(void) setStbcDelegate:(id<ServerPickerDelegate>)newDelegate;
@end

@implementation ServerTabBarController

@synthesize serverBrowser, manualEntry, stbcDelegate, browserNav, manualNav;

#pragma mark -
#pragma mark Protocols
-(void) setStbcDelegate:(id<ServerPickerDelegate>)newDelegate {
    if (self.serverBrowser != nil) {
        self.serverBrowser.delegate = newDelegate;
    }
    if (self.manualEntry != nil) {
        self.manualEntry.delegate = newDelegate;
    }
    
    stbcDelegate = newDelegate;
}

#pragma mark -
#pragma mark Custom stuff
-(void) initTBC {
    self.serverBrowser = [[ServerPickerController alloc] init];
    self.browserNav = [[UINavigationController alloc] initWithRootViewController:self.serverBrowser];
    self.browserNav.tabBarItem.title = @"Browser";
    self.browserNav.tabBarItem.image = [UIImage imageNamed:@"BonjourTab.png"];
    self.serverBrowser.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self.serverBrowser action:@selector(cancelAction)];
    self.serverBrowser.navigationItem.title = @"Server Browser";
    
    self.manualEntry = [[ServerEntryController alloc] init];
    self.manualNav = [[UINavigationController alloc] initWithRootViewController:self.manualEntry];
    self.manualEntry.tabBarItem.title = @"Manual";
    self.manualEntry.tabBarItem.image = [UIImage imageNamed:@"pencilangled.png"];
    
    self.manualEntry.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self.manualEntry action:@selector(cancelSelection)];
    self.manualEntry.navigationItem.title = @"Manual Entry";
    
    self.viewControllers = [NSArray arrayWithObjects:self.browserNav, self.manualNav, nil];
    self.modalPresentationStyle = UIModalPresentationFormSheet;
}

#pragma mark -
#pragma mark NSObject
-(id) init {
    self = [super init];
    if (self) {
        [self initTBC];
    }
    return self;
}

@end
