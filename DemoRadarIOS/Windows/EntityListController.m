 //
//  EntityListController.m
//  DemoRadar
//
//  Created by Nick Fajardo on 3/27/11.
//  Copyright 2011 Nick Fajardo. All rights reserved.
//
#import "EntityListController.h"

@implementation EntityListController
@synthesize entityDict = _entityDict, delegate;

#pragma mark -
#pragma mark Custom Stuff
-(void) refreshDisplay {
    [self.tableView reloadData];
}

#pragma mark -
#pragma mark UITableViewDataSource implementation
-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}
-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return self.entityDict.dict.count;
}
-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    DemoEntity *tmpEntity = [self.entityDict entityFromIndex:indexPath];
    if (tmpEntity != nil) {
        cell.textLabel.text = [NSString stringWithFormat:@"%@", tmpEntity.entityName];
        if (tmpEntity.entityTypeName != nil) {
            cell.textLabel.text = [cell.textLabel.text stringByAppendingFormat:@" (%@)", tmpEntity.entityTypeName];
        }
        
        cell.detailTextLabel.text = [NSString stringWithFormat:@"Obj #%@", tmpEntity.objectID];
        if (tmpEntity.selected) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        } else {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    }
    return cell;
}

#pragma mark -
#pragma mark UITableViewDelegate implementation
-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	DemoEntity *tmpEntity = [self.entityDict entityFromIndex:indexPath];
    if (tmpEntity != nil) {
        tmpEntity.selected = !tmpEntity.selected;
        
        [self.delegate didSelectEntityFromListWithObjID:tmpEntity.objectID];
        [self refreshDisplay];
	}
}

#pragma mark -
#pragma mark UIViewController override
-(BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    return YES;
}
-(void) viewDidLoad {
    [super viewDidLoad];
	self.title = @"Entity List";
	
	[self.tableView flashScrollIndicators];
}
-(void) didReceiveMemoryWarning {
    self.entityDict = nil;
	
    [super didReceiveMemoryWarning];
}

#pragma mark -
#pragma mark NSObject override

@end