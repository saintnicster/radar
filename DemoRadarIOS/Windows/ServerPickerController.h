//
//  ServerPickerController.h
//  DemoRadar
//
//  Created by Nick Fajardo on 3/26/11.
//  Copyright 2011 Nick Fajardo. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "ServerPickerDelegate.h"
#import "Reachability.h"

@interface ServerPickerController : UIViewController <NSNetServiceBrowserDelegate, NSNetServiceDelegate, UIAlertViewDelegate, UITableViewDelegate, UITableViewDataSource> {
	id <ServerPickerDelegate> __unsafe_unretained delegate;
	UIBarButtonItem *cancelButton;
	UITableView *tableView;
	
	NSNetServiceBrowser	*_browser;
	NSMutableArray *_foundServices;
    
    Reachability* localWifiReachable;
}
@property (unsafe_unretained) id <ServerPickerDelegate> delegate;
@property (nonatomic, strong) IBOutlet UIBarButtonItem *cancelButton;
@property (nonatomic, strong) IBOutlet UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *foundServices;

-(IBAction) cancelAction;
-(IBAction) connectToServer;

-(void) checkNetworkStatus:(Reachability *)notice;


@end
