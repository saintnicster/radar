//
//  ServerPickerDelegate.h
//  DemoRadar
//
//  Created by Nick Fajardo on 9/24/11.
//  Copyright 2011 Nick Fajardo. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ServerPickerDelegate
-(void) didSelectHostname:(NSString *)hostname andPort:(NSInteger)portNumber;
-(void) didCancelServerSelection;
@end