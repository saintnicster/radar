//
//  MapCanvas.h
//  DemoRadar
//
//  Created by Nick Fajardo on 7/23/11.
//  Copyright 2011 Nick Fajardo. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "EntityDict.h"
#import "EntityCanvasMarker.h"

@protocol MapCanvasDelegate
-(void) didSelectEntity:(DemoEntity *)entity;
@end

@interface MapCanvas : UIScrollView <UIScrollViewDelegate, EntityCanvasMarkerDelegate> {
    EntityDict *_dict;
	UIView *_containerView;
}
@property (strong) EntityDict *dict;
@property (strong) UIView *containerView;
@property (nonatomic) CGSize unzoomedContentSize;

-(void) clearMap;
-(void) displayEntities;
-(void) resetContentSize;
-(void) setEntityForRedisplay:(NSString *)entityID;
@end