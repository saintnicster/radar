//
//  MapViewController.h
//  DemoRadar
//
//  Created by Nick Fajardo on 3/25/11.
//  Copyright 2011 Nick Fajardo. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "UIViewControllerExtension.h"
#import "EntityListController.h"
#import "MapCanvas.h"
#import "MVRightBBIWrapper.h"
#import "RadarClient.h"
#import "ServerTabBarController.h"
#import "ServerPickerDelegate.h"

#import "AboutViewController.h"

@interface MapViewController : UIViewController <UIActionSheetDelegate, ServerPickerDelegate, MVRBBIWDelegate, EntityListDelegate, RadarClientDelegate, MapCanvasDelegate, AboutViewControllerDelegate> {
    UIBarButtonItem *displayList;
    MapCanvas *mapView;
    EntityListController *_entityList;
	MVRightBBIWrapper *_bbiWrapper;
    RadarClient *_fileGetter;
    
    ServerTabBarController *tabBar;
    
    UIPopoverController *entityPopover;
    UINavigationController *entityListNav;
    UIActivityIndicatorView *progressSpinner;
}
@property (nonatomic, strong) IBOutlet UIBarButtonItem *displayList;
@property (nonatomic, strong) IBOutlet MapCanvas *mapView;

@property (strong) MVRightBBIWrapper *bbiWrapper;
@property (strong) UIPopoverController *entityPopover;
@property (strong) UIActivityIndicatorView *progressSpinner;
@property (strong) UINavigationController *entityListNav;
@property (strong) ServerTabBarController *tabBar;
@property (unsafe_unretained, readonly) EntityListController *entityList;
@property (unsafe_unretained, readonly) RadarClient *fileGetter;
@property (readonly) BOOL isPad;

-(void) lookForServers;
-(void) appEnteringBackground;
-(void) appEnteringForeground;

-(IBAction) reloadFile;
-(IBAction) showEntityList:(id)sender;

@end