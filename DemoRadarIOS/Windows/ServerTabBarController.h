//
//  ServerTabBarController.h
//  DemoRadar
//
//  Created by Nick Fajardo on 9/24/11.
//  Copyright 2011 Nick Fajardo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServerPickerController.h"
#import "ServerEntryController.h"

@interface ServerTabBarController : UITabBarController {
    id <ServerPickerDelegate> __unsafe_unretained stbcDelegate;
    
    ServerPickerController *serverBrowser;
    ServerEntryController  *manualEntry;
    
    UINavigationController *browserNav;
    UINavigationController *manualNav;
}

@property (strong) ServerPickerController *serverBrowser;
@property (strong) ServerEntryController *manualEntry;
@property (nonatomic, unsafe_unretained) id<ServerPickerDelegate> stbcDelegate;

@property (strong) UINavigationController *browserNav;
@property (strong) UINavigationController *manualNav;

@end
