//
//  MVRightBBIWrapper.h
//  DemoRadar
//
//  Created by Nick Fajardo on 7/29/11.
//  Copyright 2011 Nick Fajardo. All rights reserved.
//
#import "MVRightBBIWrapper.h"
@interface MVRightBBIWrapper()

-(void) drawMapAction;
-(void) actionSheetAction;
@end

@implementation MVRightBBIWrapper
@synthesize delegate, barButtonItemView = _barButtonItemView, actionButton = _actionButton, parseButton = _parseButton;

#pragma mark -
#pragma mark Custom Methods
-(void) drawMapAction {
	[self.delegate userDidSelectButton:MVRBBIWButtonTypeParse];
}
-(void) actionSheetAction {
	[self.delegate userDidSelectButton:MVRBBIWButtonTypeAction];
}

#pragma mark -
#pragma mark NSObject
-(id) init {
	self = [super init];
	
	if (self) {
		ToolbarNoBG *toolbar = [[ToolbarNoBG alloc] initWithFrame:CGRectMake(0, 0, 133, 44.01)];
		
        self.actionButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction
                                                                           target:self
                                                                           action:@selector(actionSheetAction)];
        UIBarButtonItem *fixedSpace = nil;
        fixedSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace 
                                                                   target:nil 
                                                                   action:nil];
        
		self.parseButton = [[UIBarButtonItem alloc] initWithTitle:@"Draw Map"
                                                             style:UIBarButtonItemStyleDone
                                                            target:self
                                                            action:@selector(drawMapAction)];
		
		toolbar.items = [NSArray arrayWithObjects:self.actionButton, fixedSpace, self.parseButton,nil];
		
		self.barButtonItemView = [[UIBarButtonItem alloc] initWithCustomView:toolbar];
	}
	return self;
}
@end