//
//  ServerEntryController.m
//  DemoRadar
//
//  Created by Nick Fajardo on 9/24/11.
//  Copyright 2011 Nick Fajardo. All rights reserved.
//

#import "ServerEntryController.h"

@implementation ServerEntryController

@synthesize hostAddress, port, delegate;

#pragma mark - 
#pragma mark Custom
-(IBAction) connectToServer {
    [delegate didSelectHostname:hostAddress.text andPort:port.text.integerValue];
}
-(IBAction) cancelSelection {
    [delegate didCancelServerSelection];
}

#pragma mark -
#pragma mark UIViewController
-(BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return YES;
}

@end
