//
//  AboutViewController.m
//  RadarIOS
//
//  Created by Nick Fajardo on 11/12/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "AboutViewController.h"

static NSString *const kABOUT_URL = @"https://bitbucket.org/saintnicster/radar/raw/master/DemoRadarIOS/about.html";

@interface AboutViewController() {    
    UIActivityIndicatorView *_spinner;
}

@property (unsafe_unretained, readonly) UIActivityIndicatorView *spinner;

-(void) clearWebView;
-(void) loadPage;
-(void) loadPageLocally;
-(void) close;
@end

@implementation AboutViewController

#pragma mark -
#pragma mark Properties
@synthesize avcDelegate = _avcDelegate;
-(UIWebView *) webView {
    if (_webView == nil) { 
        _webView = [[UIWebView alloc] init];
        _webView.delegate = self;
    }
    
    return _webView;
}
-(UIViewController *) vc {
    if (_vc == nil) {
        _vc = [[UIViewController alloc] init];
        _vc.view = self.webView;
        _vc.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(close)];
        _vc.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.spinner];
        _vc.title = @"About";
    }
    return _vc;
}
-(UIActivityIndicatorView *)spinner {
    if (_spinner == nil) {
        _spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        if ([_spinner respondsToSelector:@selector(color)]) {
            _spinner.color = [UIColor blackColor];
        }
        _spinner.hidesWhenStopped = YES;
    }
    return _spinner;
}

#pragma mark -
#pragma mark Custom
-(void) clearWebView {
    _webView = nil;
}
-(void) loadPage {
    [self.spinner startAnimating];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:kABOUT_URL] cachePolicy:NSURLCacheStorageAllowedInMemoryOnly timeoutInterval:5.0];
    [self.webView loadRequest:request];
}
-(void) loadPageLocally {
    NSString *path = [[NSBundle mainBundle] pathForResource:@"about" ofType:@"html"];
    NSData *aboutFile = nil;
    if (path) {
        aboutFile = [NSData dataWithContentsOfFile:path];
    }
    if (aboutFile) {
        [self.webView loadData:aboutFile MIMEType:@"text/html" textEncodingName:@"UTF-8" baseURL:[NSURL URLWithString:@"http://bitbucket.org/saintnicster/radar/"]];
    } else {
        [self.spinner stopAnimating];
    }
}
-(void) close {
    if (self.avcDelegate != nil) {
        [self.avcDelegate closeButtonPressed];
    }
}

#pragma mark -
#pragma mark UIWebViewDelegate
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    if (navigationType == UIWebViewNavigationTypeLinkClicked) {
        [[UIApplication sharedApplication] openURL:[request URL]];
        return NO;
    }
    return YES;
}
-(void)webViewDidStartLoad:(UIWebView *)webView {
    self.spinner.hidden = NO;
    [self.spinner startAnimating];
}
-(void)webViewDidFinishLoad:(UIWebView *)webView {
    [self.spinner stopAnimating];
}
-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    NSLog(@"%i", error.code);
    if (error.code == -1009) {
        if ([[error.userInfo objectForKey:NSURLErrorFailingURLStringErrorKey] isEqualToString:kABOUT_URL]) {
            [self loadPageLocally];
        }
    } else {
        [self.spinner stopAnimating];
    }
}

#pragma mark -
#pragma mark UIViewController
-(void)viewDidAppear:(BOOL)animated {
    [self loadPage];
}
-(void) didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    [self clearWebView];
}
-(BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return YES;
}


#pragma mark -
#pragma mark NSObject
-(id) init {
    self = [super init];
    if (self) {
        self.viewControllers = [NSArray arrayWithObject:self.vc];
        
        self.modalPresentationStyle = UIModalPresentationFormSheet;
    }
    return self;
}

@end
