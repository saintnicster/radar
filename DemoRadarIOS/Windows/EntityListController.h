//
//  EntityListController.h
//  DemoRadar
//
//  Created by Nick Fajardo on 3/27/11.
//  Copyright 2011 Nick Fajardo. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "EntityDict.h"
#import "DemoEntity.h"

@protocol EntityListDelegate
-(void) didSelectEntityFromListWithObjID:(NSString *)objID;
@end

@interface EntityListController : UITableViewController {
    id <EntityListDelegate> __unsafe_unretained delegate;
    EntityDict *_entityDict;
    NSArray *_entityListTable;
}
@property (unsafe_unretained) id <EntityListDelegate>  delegate;
@property (strong) EntityDict *entityDict;

-(void) refreshDisplay;

@end