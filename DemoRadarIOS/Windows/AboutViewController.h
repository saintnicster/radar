//
//  AboutViewController.h
//  RadarIOS
//
//  Created by Nick Fajardo on 11/12/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AboutViewControllerDelegate
-(void)closeButtonPressed;
@end

@interface AboutViewController : UINavigationController <UIWebViewDelegate> {
    UIViewController *_vc;
    id<AboutViewControllerDelegate> __unsafe_unretained _avcDelegate;
    UIWebView *_webView;
}
@property (unsafe_unretained, readonly) UIViewController *vc;
@property (unsafe_unretained, readonly) UIWebView *webView;
@property (unsafe_unretained) id<AboutViewControllerDelegate> avcDelegate;

@end
