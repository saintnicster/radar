//
//  MapViewController.m
//  DemoRadar
//
//  Created by Nick Fajardo on 3/25/11.
//  Copyright 2011 Nick Fajardo. All rights reserved.
//
#import "MapViewController.h"

@interface MapViewController() {
    UIActionSheet *_actionSheet;

    
    BOOL shouldGetMapAfterLookup;
    
    Class modalPopoverBeforeBackground;
    
    BOOL parsingFile;
}

@property (strong) UIActionSheet *actionSheet;

-(void) createAboutScreen;
-(void) clickActionButton;
-(void) clickParseButton;
-(void) spinnerOn;
-(void) spinnerOff;
-(void) toggleActionSheet;

@end

@implementation MapViewController

#pragma mark -
#pragma mark Properties
@synthesize progressSpinner;
@synthesize mapView, displayList, entityPopover, entityListNav, tabBar;
@synthesize bbiWrapper = _bbiWrapper, actionSheet = _actionSheet;
-(EntityListController *) entityList {
    if (_entityList == nil) {
        _entityList = [[EntityListController alloc] init];
        _entityList.delegate = self;
    }
    return _entityList;
}
-(RadarClient *) fileGetter {
	if (_fileGetter == nil) {
        _fileGetter = [[RadarClient alloc] init]; 
        _fileGetter.delegate = self; 
    }
	return _fileGetter;
}
-(BOOL) isPad {
	BOOL isPad = NO;
	if ([[UIDevice currentDevice] respondsToSelector:@selector(userInterfaceIdiom)]) {
		isPad = ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad);
	}
    return isPad;
}

#pragma mark -
#pragma mark Custom Methods
-(void) createAboutScreen {
    AboutViewController *avc = [[AboutViewController alloc] init];
    avc.avcDelegate = self;
    
    [self openModalWindow:avc Animated:YES];
    
}
-(void) clickActionButton {
    [self userDidSelectButton:MVRBBIWButtonTypeAction];
}
-(void) clickParseButton {
    [self userDidSelectButton:MVRBBIWButtonTypeParse];
}
-(void) appEnteringBackground {
    UIViewController *tempVC = nil;
    if ([self respondsToSelector:@selector(presentedViewController)]) {
        tempVC = self.presentedViewController;
    } else {
        tempVC = self.modalViewController;
    }
    
    if (tempVC != nil) {        
        if ( [tempVC isKindOfClass:[AboutViewController class]] ) {
            modalPopoverBeforeBackground = [AboutViewController class];
        } else if ( [tempVC isKindOfClass:[ServerTabBarController class]] ) {
            modalPopoverBeforeBackground = [ServerTabBarController class];
        }
    }
    
    [self closeModalWindowAnimated:NO];
}
-(void) appEnteringForeground {
    if (modalPopoverBeforeBackground) {
        if (modalPopoverBeforeBackground == [AboutViewController class]) {
            [self createAboutScreen];
        } else if (modalPopoverBeforeBackground == [ServerTabBarController class]) {
            [self lookForServers];
        }
    }
    
    modalPopoverBeforeBackground = nil;
}
-(void) lookForServers {
	if (self.entityPopover.popoverVisible) {
        [self.entityPopover dismissPopoverAnimated:YES];
        self.entityPopover = nil;
    }
    self.tabBar = [[ServerTabBarController alloc] init];
    self.tabBar.stbcDelegate = self;
    
    [self openModalWindow:self.tabBar Animated:YES];
}
-(void) spinnerOn {
    if (self.progressSpinner == nil) {
        self.progressSpinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        self.progressSpinner.frame = CGRectMake(0, 0, 40, 40);
        self.progressSpinner.center = self.view.center;
        
        [self.view addSubview:progressSpinner];
        [progressSpinner startAnimating];
    }
}
-(void) spinnerOff {
    if (self.progressSpinner != nil) {
        if (self.progressSpinner.isAnimating) {
            [self.progressSpinner stopAnimating];
        }
        
        [self.progressSpinner removeFromSuperview];
        self.progressSpinner = nil;
    }
}
-(void) toggleActionSheet {
    if (self.entityPopover.popoverVisible) {
        [self.entityPopover dismissPopoverAnimated:YES];
        self.entityPopover = nil;
    }
    
    if (self.actionSheet.visible) { 
        [self.actionSheet dismissWithClickedButtonIndex:self.actionSheet.cancelButtonIndex animated:YES]; 
    } else {
        if (self.actionSheet == nil) {
            self.actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                            delegate:self
                                                   cancelButtonTitle:@"Cancel Button"
                                              destructiveButtonTitle:nil
                                                   otherButtonTitles:@"Connect to Server", @"About Page", nil];
        }
        if (self.isPad) {
            [self.actionSheet showFromBarButtonItem:self.bbiWrapper.actionButton animated:YES];
        } else {
            [self.actionSheet showFromToolbar:self.navigationController.toolbar];
        }
    }
}

#pragma mark -
#pragma mark IBAction Methods
-(IBAction) showEntityList:(id)sender {
    if (self.entityPopover.popoverVisible) {
        [self.entityPopover dismissPopoverAnimated:YES];
        self.entityPopover = nil;
    } else {
        if (self.entityListNav == nil) {
            self.entityListNav = [[UINavigationController alloc] initWithRootViewController:self.entityList];
        }
        [self.entityList refreshDisplay];
        
        if (self.isPad) {
            self.entityPopover = [[UIPopoverController alloc] initWithContentViewController:self.entityListNav];
            
            [self.entityPopover presentPopoverFromBarButtonItem:sender 
                                       permittedArrowDirections:UIPopoverArrowDirectionAny 
                                                       animated:YES];
        } else {
            [self.navigationController pushViewController:self.entityList animated:YES];
        }
        
        if (self.actionSheet.visible) { 
            [self.actionSheet dismissWithClickedButtonIndex:self.actionSheet.cancelButtonIndex animated:YES]; 
        }
    }
}
-(IBAction) reloadFile {
    if (self.fileGetter.hostIsSet) {
        [self spinnerOn];
        
        [self.fileGetter getDemoFromHost];
    } else {
        shouldGetMapAfterLookup = YES;
        
        [self lookForServers];
    }
    
}

#pragma mark -
#pragma mark UIActionSheetDelegate Implementation
-(void) actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
	switch (buttonIndex) {
        case 0:
            [self lookForServers];
            break;
        case 1:
            [self createAboutScreen];
            break;
        default:
            break;
    }
}

#pragma mark -
#pragma mark ServerPickerDelegate implementation
-(void) didSelectHostname:(NSString *)hostname andPort:(NSInteger)portNumber {
	[self.fileGetter setHostName:hostname andPort:portNumber];
    
    [self closeModalWindowAnimated:YES];
    
    self.tabBar = nil;
    
    if (shouldGetMapAfterLookup) {
        shouldGetMapAfterLookup = NO;
        [self reloadFile];
    }
}

-(void) didCancelServerSelection {
    [self closeModalWindowAnimated:YES];
    
    self.tabBar = nil;
    shouldGetMapAfterLookup = NO;
}

#pragma mark -
#pragma mark MVRBBIWDelegate Implementation
-(void) userDidSelectButton:(MVRBBIWButtonType)buttonType {
	if (buttonType == MVRBBIWButtonTypeAction) {
        [self toggleActionSheet];
    } else if (buttonType == MVRBBIWButtonTypeParse) {
        if (!parsingFile) {
            parsingFile = YES;
            if (self.actionSheet.visible) {
                [self.actionSheet dismissWithClickedButtonIndex:self.actionSheet.cancelButtonIndex animated:YES];
            }
            if (self.entityPopover.popoverVisible) {
                [self.entityPopover dismissPopoverAnimated:YES];
                self.entityPopover = nil;
            }
            [self reloadFile];
            parsingFile = NO;
        }
	}
}

#pragma mark -
#pragma mark RadarClientDelegate
-(void) didReceiveEntityList:(NSArray *)listOfEntities {
	EntityDict *tmpDict = [[EntityDict alloc] init];
    NSMutableArray *keyArr = [[NSMutableArray alloc] initWithCapacity:listOfEntities.count];
    
	for (NSDictionary *dict in listOfEntities) {
		DemoEntity *tmpEntity = [DemoEntity entityFromNSDictionary:dict];
        
        if (tmpEntity != nil && tmpEntity.entityName != nil) {
            [tmpDict addEntity:tmpEntity];
            
            [keyArr addObject:tmpEntity.objectID];
        }
	}
    
    self.entityList.entityDict = tmpDict;
    [self.entityList.tableView reloadData];
    [self.entityList.tableView flashScrollIndicators];
    
    [self.mapView clearMap];
    self.mapView.dict = tmpDict;
    [self.mapView displayEntities];
    [self.mapView setNeedsLayout];
    
    [self spinnerOff];
}
-(void) errorConnectingToServerWithMessage:(NSString *)errorMsg {
    [self spinnerOff];
    [self.fileGetter setHostName:@"" andPort:NSIntegerMin];
    [self reloadFile];
    
	[[[UIAlertView alloc] initWithTitle: @"Connection Error" 
								 message: errorMsg 
								delegate: nil 
					   cancelButtonTitle: @"OK" 
					   otherButtonTitles: nil] show];
    
}
-(void) errorReadngFileWithMessage:(NSString *)errorMsg {
    [self spinnerOff];
    
	[[[UIAlertView alloc] initWithTitle: @"File Error" 
								 message: errorMsg 
								delegate: nil 
					   cancelButtonTitle: @"OK" 
					   otherButtonTitles: nil] show];
    
}

#pragma mark -
#pragma mark EntityListDelegate Implementation
-(void) dismissEntityList {
    if ([self closeModalWindowAnimated:YES] == NO) {
        if (self.navigationController.visibleViewController == self.entityList) {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}
-(void) didSelectEntityFromListWithObjID:(NSString *)objID {
    [self dismissEntityList];
    
    [self.mapView setEntityForRedisplay:objID];
}

#pragma mark -
#pragma mark MapCanvasDelegate
-(void) didSelectEntity:(DemoEntity *)entity {
    [self dismissEntityList];
}

#pragma mark -
#pragma mark AboutViewControllerDelegate
-(void) closeButtonPressed {
    [self closeModalWindowAnimated:YES];
}


#pragma mark -
#pragma mark UIViewController overrides
-(BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    return YES;
}
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    self.mapView.unzoomedContentSize = self.view.frame.size;
}
-(void) viewDidLoad {
    [super viewDidLoad];
    
    if (self.isPad) {
        if (self.bbiWrapper == nil) {
            self.bbiWrapper = [[MVRightBBIWrapper alloc] init];
            self.bbiWrapper.delegate = self;
        }
        self.navigationItem.rightBarButtonItem = self.bbiWrapper.barButtonItemView;
    } else {
        self.navigationController.toolbarHidden = NO;
        
        self.toolbarItems = [NSArray arrayWithObjects:[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil], [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(clickActionButton)], nil];
        
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Draw Map" style:UIBarButtonItemStyleDone target:self action:@selector(clickParseButton)];
    }
    self.navigationItem.leftBarButtonItem = self.displayList;
    self.navigationItem.title = @"CityRadar";
    
    
}
-(void) viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
    
    [self.mapView resetContentSize];
}
-(void) didReceiveMemoryWarning {
    [self.mapView clearMap];
	
    [super didReceiveMemoryWarning];
}

@end