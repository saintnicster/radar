Demo Radar is an external mapping program to be used with City of Heroes(c).

Use any cohdemo file to create a basic map/layout of your character's surroundings.

** External Libraries and Assets **
DemoRadar logo and icon created by Liz Chesterman (http://imaginatorcentral.com) and used with permission
Apple Bonjour/mDSNResponder (zeroconf) - http://developer.apple.com/opensource/
"plist" Java Property List Library - http://code.google.com/p/plist/
Eclipse Standard Widget Toolkit (SWT) - http://www.eclipse.org/swt/
Reachability 2.0.4ddg - http://blog.ddg.com/?p=24
app-bits icons (for the pencil icon) - http://www.app-bits.com/free-icons.html

***This is a third-party application created without the permission/endorsement of NCSoft and NC Interactive.***

City of Heroes and its related properties are owned by NC Interactive