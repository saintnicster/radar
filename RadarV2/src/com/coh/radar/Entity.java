package com.coh.radar;

import java.util.EnumMap;

import com.dd.plist.*;

public class Entity implements Comparable<Entity> {

	public EntityType entityType;
	public String objectID;
	public String entityName;
	public boolean entityIsDead = false;
	public boolean selected = false;
	
	public float 	posXAxis = 0.0F, 
					posYAxis = 0.0F,
					posZAxis = 0.0F;
	
	public static enum EntityType {
		PLAYER, TEAMMATE, GLOWIE, DESTRUCT, ANTIGLOWIE, 
		CONTACT_HAS_MISH, CONTACT_INPROG, CONTACT_MISSIONOVER,
		CAPTIVE, HOSTAGE, QUANTUM, DOOR, PUDDLE, NONE
	}
	public static EnumMap<EntityType, String> TYPE_DISPLAY_NAME;
	
	static {
		TYPE_DISPLAY_NAME = new EnumMap<Entity.EntityType, String>(EntityType.class);
		TYPE_DISPLAY_NAME.put(EntityType.PLAYER, Messages.getString("Entity.typePlayerCharacter")); 				//$NON-NLS-1$
		TYPE_DISPLAY_NAME.put(EntityType.TEAMMATE, Messages.getString("Entity.typeOtherPCOrAECritter"));			//$NON-NLS-1$
		TYPE_DISPLAY_NAME.put(EntityType.GLOWIE, Messages.getString("Entity.typePlainGlowie"));						//$NON-NLS-1$
		TYPE_DISPLAY_NAME.put(EntityType.DESTRUCT, Messages.getString("Entity.typeGlowieWithHP"));					//$NON-NLS-1$
		TYPE_DISPLAY_NAME.put(EntityType.ANTIGLOWIE, Messages.getString("Entity.typeInvisibleGlowie"));				//$NON-NLS-1$
		TYPE_DISPLAY_NAME.put(EntityType.CONTACT_HAS_MISH, Messages.getString("Entity.typeContactOpenMish"));		//$NON-NLS-1$
		TYPE_DISPLAY_NAME.put(EntityType.CONTACT_INPROG, Messages.getString("Entity.typeContactActiveMish"));		//$NON-NLS-1$
		TYPE_DISPLAY_NAME.put(EntityType.CONTACT_MISSIONOVER, Messages.getString("Entity.typeContactDoneMish"));	//$NON-NLS-1$
		TYPE_DISPLAY_NAME.put(EntityType.CAPTIVE, Messages.getString("Entity.typeSacrifice"));						//$NON-NLS-1$
		TYPE_DISPLAY_NAME.put(EntityType.HOSTAGE, Messages.getString("Entity.typeNPCFear"));						//$NON-NLS-1$
		TYPE_DISPLAY_NAME.put(EntityType.QUANTUM, Messages.getString("Entity.typeNictus"));							//$NON-NLS-1$
		TYPE_DISPLAY_NAME.put(EntityType.DOOR, Messages.getString("Entity.typeDoor"));								//$NON-NLS-1$
		TYPE_DISPLAY_NAME.put(EntityType.PUDDLE, Messages.getString("Entity.typePuddle"));							//$NON-NLS-1$
		TYPE_DISPLAY_NAME.put(EntityType.NONE, Messages.getString("Empty"));										//$NON-NLS-1$
	}
	
	Entity(String objectID) {
		this(objectID, Messages.getString("Empty"));		 //$NON-NLS-1$
		
	}
	Entity(String objectID, String entityName) {
		this.objectID	= objectID;
		this.entityName = entityName;
		
		this.entityType = EntityType.NONE;
	}
	
	@Override
	public String toString() {
		return objectID +"-"+ entityName +Messages.getString("NewLineCharacter")+ posXAxis +", "+ posZAxis +", "+ posYAxis ; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
	}
	@Override
	public int compareTo(Entity compEntity) {
		int returnValue = 0;
		
		returnValue = this.compareType(compEntity);
		if (returnValue == 0) {
			returnValue = this.compareName(compEntity);
		}
		if (returnValue == 0) {
			returnValue = this.objectID.compareTo(compEntity.objectID);
		}
		return returnValue;
	}

	public void setCoordinates(float posXAxis, float posZAxis, float posYAxis) {
		this.posXAxis = posXAxis;
		this.posZAxis = posZAxis;
		this.posYAxis = posYAxis;
	}
	public void setEntityName(String newEntityName) {
		this.entityName = newEntityName;
	}
	public void setEntityType(EntityType newEntityType) {
		this.entityType = newEntityType;
	}
	public void setEntityType(NSNumber newEntityType) {
		int index = newEntityType.intValue();
		try {
			this.entityType = EntityType.values()[index];
		} catch (ArrayIndexOutOfBoundsException e) {
			this.entityType = EntityType.NONE;
		}
	}
	
	
	public void makeDead() {
		this.entityIsDead = true;
	}
	public String getEntityTypeText() {
		String entityTypeText = "";
		if (TYPE_DISPLAY_NAME.containsKey(this.entityType)) {
			entityTypeText = TYPE_DISPLAY_NAME.get(this.entityType);
		}
		return entityTypeText;
	}
	
	private int compareType(Entity compEntity) {
		return this.entityType.compareTo(compEntity.entityType);
	}
	private int compareName(Entity compEntity) {
		return (this.entityName).compareTo(compEntity.entityName);
	}
	
	public void mapFromNSDictionary(NSDictionary nsDict) {
		
		setEntityName( nsDict.objectForKey(Messages.getString("Entity.plistKeyEntityName")).toString() ); //$NON-NLS-1$
		setEntityType( ((NSNumber) nsDict.objectForKey(Messages.getString("Entity.plistKeyTypeNbr"))) );   //$NON-NLS-1$
		
		if (((NSNumber) nsDict.objectForKey(Messages.getString("Entity.plistKeyDead"))).boolValue()) { makeDead(); }; //$NON-NLS-1$
		
		setCoordinates( (float) ((NSNumber) nsDict.objectForKey(Messages.getString("Entity.plistKeyXPos"))).doubleValue(), (float) ((NSNumber) nsDict.objectForKey(Messages.getString("Entity.plistKeyZPos"))).doubleValue(), (float) ((NSNumber) nsDict.objectForKey(Messages.getString("Entity.plistKeyYPos"))).doubleValue() ); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
	}
	
	public NSDictionary toNSDictionary() {
		NSDictionary returnDict = new NSDictionary();
		
		returnDict.put(Messages.getString("Entity.plistKeyObjID"), this.objectID); //$NON-NLS-1$
		returnDict.put(Messages.getString("Entity.plistKeyEntityName"), this.entityName); //$NON-NLS-1$
		returnDict.put(Messages.getString("Entity.plistKeyDead"), this.entityIsDead); //$NON-NLS-1$
		returnDict.put(Messages.getString("Entity.plistKeyTypeNbr"), this.entityType.ordinal()); //$NON-NLS-1$
		returnDict.put(Messages.getString("Entity.plistKeyTypeName"), this.getEntityTypeText()); //$NON-NLS-1$
		returnDict.put(Messages.getString("Entity.plistKeyXPos"), this.posXAxis); //$NON-NLS-1$
		returnDict.put(Messages.getString("Entity.plistKeyYPos"), this.posYAxis); //$NON-NLS-1$
		returnDict.put(Messages.getString("Entity.plistKeyZPos"), this.posZAxis); //$NON-NLS-1$
		
		return returnDict;
	}
}
