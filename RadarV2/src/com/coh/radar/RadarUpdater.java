package com.coh.radar;

import java.io.FileNotFoundException;
import java.net.URL;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Timer;
import java.util.TimerTask;


import org.eclipse.swt.SWT;
import org.eclipse.swt.program.Program;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

import com.dd.plist.*;

public class RadarUpdater {
	Timer updateTimer = null;
	MessageBox mbox = null;
	SimpleDateFormat sdf = new SimpleDateFormat(Messages.getString("RadarUpdater.DateFormatString")); //$NON-NLS-1$
	
	public RadarUpdater(Shell parentShell) {
		mbox = new MessageBox(parentShell, SWT.ICON_INFORMATION | SWT.YES | SWT.NO);
		
		updateTimer = new Timer();
		scheduleRepeatedCheck( 5*100, 24 * 60 * 1000 );
	}
	public void scheduleSingleCheck(int delay) {
		updateTimer.schedule(new UpdateTask(this), delay);
		System.out.println(("Check scheduled in &V1& ms.").replaceAll(Messages.getString("VariableOne"), delay+Messages.getString("Empty"))); //$NON-NLS-2$ //$NON-NLS-3$
	}
	public void scheduleRepeatedCheck(int delay, int repeat) {
		updateTimer.schedule(new UpdateTask(this), delay, repeat);
		System.out.println(("Check scheduled in &V1& ms.  Repeating in &V2& ms.").replaceAll(Messages.getString("VariableOne"), delay+Messages.getString("Empty")).replaceAll(Messages.getString("VariableTwo"), repeat+Messages.getString("Empty"))); //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
	}
	public MessageBox getMessageBox(NSDictionary updateDict) {
		Version updateVersion = null;
		String dateString = null;
		NSArray changeArray = null;
		NSObject buffer = null; 

		buffer = updateDict.objectForKey(Messages.getString("RadarUpdater.PList_Version_Key")); //$NON-NLS-1$
		if (buffer != null && buffer.getClass() == NSString.class) {
			updateVersion = Version.VersionFromString(updateDict.objectForKey(Messages.getString("RadarUpdater.PList_Version_Key")).toString()); //$NON-NLS-1$
		}

		buffer = null;
		buffer = updateDict.objectForKey(Messages.getString("RadarUpdater.PList_Date_Key")); //$NON-NLS-1$
		if (buffer != null && buffer.getClass() == NSDate.class) {
			dateString = sdf.format(((NSDate) buffer).getDate());
		}

		String message = Messages.getString("RadarUpdater.Found_Header")+Messages.getString("RadarUpdater.NewLine")+Messages.getString("RadarUpdater.NewLine"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		message = message.concat((Messages.getString("RadarUpdater.CurrentVersion_Label")+Messages.getString("RadarUpdater.NewLine")).replaceAll(Messages.getString("VariableOne"), RadarConsts.VERSION.toString())); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		message = message.concat((Messages.getString("RadarUpdater.NewVersion_Label")+Messages.getString("RadarUpdater.NewLine")).replaceAll(Messages.getString("VariableOne"), updateVersion.toString())); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		if (dateString != null && !dateString.equals(Messages.getString("Empty"))) { //$NON-NLS-1$
			message = message.concat((Messages.getString("RadarUpdater.ReleaseDate_Label")+Messages.getString("RadarUpdater.NewLine")).replaceAll(Messages.getString("VariableOne"), dateString)); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		}

		buffer = null;
		buffer = updateDict.objectForKey(Messages.getString("RadarUpdater.PList_ChangeList_Key")); //$NON-NLS-1$
		if (buffer != null && buffer.getClass() == NSArray.class) {
			message = message.concat(Messages.getString("RadarUpdater.NewLine")+Messages.getString("RadarUpdater.ChangeList_Header")+Messages.getString("RadarUpdater.NewLine")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			changeArray = (NSArray) buffer;
			for (int i = 0; i < changeArray.count(); i++) {
				buffer = changeArray.objectAtIndex(i);
				if (buffer != null && buffer.getClass() == NSString.class) {
					String changeText = ((NSString) buffer)+Messages.getString("Empty"); 
					message = message.concat( (Messages.getString("RadarUpdater.ChangeList_Display_Line")+Messages.getString("RadarUpdater.NewLine")).replaceAll(Messages.getString("VariableOne"), changeText)); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
				}
			}
		}
		
		message = message.concat(new String(Messages.getString("RadarUpdater.Download_Prompt")).replaceAll(Messages.getString("VariableOne"), RadarConsts.PROGRAM_NAME)); //$NON-NLS-1$ //$NON-NLS-2$

		mbox.setText(Messages.getString("RadarUpdater.UpdateMessageBox_Title")); //$NON-NLS-1$
		mbox.setMessage(message);
		
		return mbox;
	}
	public void doUpdate(String url)  {
		Program.launch(url);
	}
	public void stopUpdater() {
		if (updateTimer != null) {
			updateTimer.cancel();
		}
		updateTimer = null;
	}
	
	public static NSDictionary getRemoteDictionary() throws RadarException {
		NSObject list = null;

		try {
			URL oracle = new URL("https://bitbucket.org/saintnicster/radar/raw/master/radar_version.plist"); //$NON-NLS-1$
			list = PropertyListParser.parse(oracle.openStream());
		} catch (Exception e) {
			Class<? extends Exception> exceptionClass = e.getClass();
			String messageTxt = null;
			if (exceptionClass == UnknownHostException.class) {
				messageTxt = Messages.getString("RadarUpdater.Cant_Connect"); //$NON-NLS-1$
			} else if (exceptionClass == FileNotFoundException.class) {
				messageTxt = Messages.getString("RadarUpdater.File_Doesnt_Exist"); //$NON-NLS-1$
			} else if (exceptionClass == UnsupportedOperationException.class) {
				messageTxt = Messages.getString("RadarUpdater.File_Wrong_Format"); //$NON-NLS-1$
			} else {
				messageTxt = e.getLocalizedMessage();
			}

			throw new RadarException(messageTxt, e);
		}
		NSDictionary retVal = null;
		if (list != null && list.getClass() == NSDictionary.class) {
			NSObject temp = ((NSDictionary) list).objectForKey(Messages.getString("RadarUpdater.PList_Header_Key")); //$NON-NLS-1$
			if (temp != null && temp.getClass() == NSDictionary.class) {
				retVal = (NSDictionary) temp;
			}
		}
		return retVal;
	}
}
class UpdateTask extends TimerTask {
	RadarUpdater parent = null;
	MessageBox mbox = null;
	int messageResponse;
	
	UpdateTask( RadarUpdater newParent) {
		this.parent = newParent;
	}
	@Override
	public void run() {
		// TODO Auto-generated method stub
		Version updateVersion = null;
		final NSDictionary updateDict;
		
		try {
			updateDict = RadarUpdater.getRemoteDictionary();
		} catch (RadarException e) {
			parent.scheduleSingleCheck( 5 * 60 * 1000 );
			return;
		}
		NSObject buffer = updateDict.objectForKey(Messages.getString("RadarUpdater.PList_Version_Key")); //$NON-NLS-1$
		if (buffer != null && buffer.getClass() == NSString.class) {
			updateVersion = Version.VersionFromString(updateDict.objectForKey(Messages.getString("RadarUpdater.PList_Version_Key")).toString()); //$NON-NLS-1$
			if (updateVersion.compareTo(RadarConsts.VERSION) == 1) {
				mbox = parent.getMessageBox(updateDict);
				mbox.getParent().getDisplay().asyncExec(new Runnable() {
					@Override
					public void run() {
						messageResponse = UpdateTask.this.mbox.open();
						switch (messageResponse) {
						case SWT.YES:
							parent.doUpdate(updateDict.objectForKey(Messages.getString("RadarUpdater.PList_URL_Key")).toString()); //$NON-NLS-1$
							parent.stopUpdater();
							UpdateTask.this.mbox.getParent().dispose();
							break;
						case SWT.NO:
							break;
						default:
							System.out.println(messageResponse);
							break;
						}
					}
				});
			}
		}
	}
}