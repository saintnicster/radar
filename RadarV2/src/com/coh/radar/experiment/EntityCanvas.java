	package com.coh.radar.experiment;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Canvas;

import com.coh.radar.Entity;
import com.coh.radar.Entity.EntityType;
//import com.coh.radar.FloatXY;
import com.swtdesigner.SWTResourceManager;

public class EntityCanvas extends Canvas {
	public static final int ENTITY_CANVAS_WIDTH  = 12;
	public static final int ENTITY_CANVAS_HEIGHT = 12;
	public static final int ICON_RADIUS = 5;
	
	int zMidPoint = Integer.MIN_VALUE;
	
	PaintListener painter = null;
	
	protected Entity baseEntity;
	public EntityCanvas(Canvas parent, int zMidPoint, Entity entity) {
		super(parent, SWT.NONE);
		// TODO Auto-generated constructor stub
		setEntity(entity);
		setBackground( SWTResourceManager.getColor(SWT.NO_BACKGROUND) );
		setSize(ENTITY_CANVAS_WIDTH, ENTITY_CANVAS_HEIGHT);
		addMouseListener(new MouseListener() {
			@Override
			public void mouseUp(MouseEvent e) {
				System.out.println("selected?");
				if (EntityCanvas.this.baseEntity != null) {
					EntityCanvas.this.baseEntity.selected = !EntityCanvas.this.baseEntity.selected;
				}
			}
			
			@Override
			public void mouseDown(MouseEvent e) {
				// TODO Auto-generated method stub
				System.out.println("mouseDown"+e);
				
			}
			
			@Override
			public void mouseDoubleClick(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
		
		painter = new PaintListener() {
			@Override
			public void paintControl(PaintEvent e) {
				// TODO Auto-generated method stub
				e.gc.setBackground( SWTResourceManager.getColor(EntityCanvas.getEntityColor(EntityCanvas.this.baseEntity.entityType)) );
				e.gc.setForeground( SWTResourceManager.getColor(SWT.COLOR_WHITE) );

				Rectangle rect = ((EntityCanvas) (e.widget)).getBounds();
				Point center = new Point(rect.width/2, rect.height/2);
				int[] pointArray = new int[8];
				if (EntityCanvas.this.baseEntity.posZAxis > (EntityCanvas.this.zMidPoint + 3)) {
					//triangle pointing down
					pointArray[0] = center.x; 				pointArray[1] = (center.y+ICON_RADIUS);
					pointArray[2] = (center.x-ICON_RADIUS); pointArray[3] = (center.y-ICON_RADIUS);
					pointArray[4] = (center.x+ICON_RADIUS); pointArray[5] = (center.y-ICON_RADIUS);
					pointArray[6] = pointArray[0];          pointArray[7] = pointArray[1];
					
					e.gc.fillPolygon(pointArray);
				} else if (EntityCanvas.this.baseEntity.posZAxis < (EntityCanvas.this.zMidPoint - 3)) {
					//Triangle pointing up
					pointArray[0] = center.x;                 pointArray[1] = (center.y - ICON_RADIUS);
					pointArray[2] = (center.x + ICON_RADIUS); pointArray[3] = (center.y + ICON_RADIUS);
					pointArray[4] = (center.x - ICON_RADIUS); pointArray[5] =(center.y + ICON_RADIUS);
					pointArray[6] = pointArray[0];            pointArray[7] = pointArray[1];

					System.out.println("test");
					
					e.gc.fillPolygon(pointArray);
				} else {
					e.gc.fillOval((center.x-ICON_RADIUS), (center.y-ICON_RADIUS), (ICON_RADIUS*2), (ICON_RADIUS*2));
				}
			}
		};
		addPaintListener(painter);
	}
	
	public Entity getEntity() {
		return this.baseEntity;
	}
	public void setEntity(Entity newEntity) {
		this.baseEntity = newEntity;
	}

//	public void setOriginPoint(Point demoMinBound, Point demoMaxBound, FloatXY scaler) {
//		int x = 0, y = 0;
//		float xF = 0.0f, yF = 0.0f;
//		Rectangle parentBounds = getParent().getBounds();
//		
////		x = (int) ( this.baseEntity.posXAxis / originalRect.width ) * parentBounds.width;
////		System.out.println("posXAxis="+this.baseEntity.posXAxis+", orig.width="+originalRect.width+", parent.width="+parentBounds.width);
////		y = (int) (( this.baseEntity.posYAxis / originalRect.height ) * parentBounds.height);
//
//		
//		xF = demoMaxBound.x - this.baseEntity.posXAxis;
//		yF = this.baseEntity.posYAxis - demoMinBound.y;
//		
//		x = (int) (xF * scaler.x);
//		y = (int) (yF * scaler.y);
//
////		System.out.println("Xf="+xF+" Yf="+yF);
////		System.out.println("Xs="+scaler.x+" Ys="+scaler.y);
////		System.out.println("X="+x+" Y="+y);
//		setLocation(x, y);
////		System.out.println(parentBounds);
//	}
	
	protected static int getEntityColor(EntityType entityType) {
		int retColorCode = SWT.COLOR_DARK_GRAY;
		switch (entityType) {
			case PLAYER:
				retColorCode = SWT.COLOR_GREEN;
				break;
			case TEAMMATE:
				retColorCode = SWT.COLOR_DARK_CYAN;
				break;
			case GLOWIE:
				retColorCode = SWT.COLOR_YELLOW;
				break;
			case DOOR:
				retColorCode = SWT.COLOR_BLUE;
				break;
			case HOSTAGE:
				retColorCode = SWT.COLOR_RED;
				break;
			case CAPTIVE:
				retColorCode = SWT.COLOR_RED;
				break;
			case ANTIGLOWIE:
				retColorCode = SWT.COLOR_YELLOW;
				break;
			case QUANTUM:
				retColorCode = SWT.COLOR_DARK_MAGENTA;
				break;
			case DESTRUCT:
				retColorCode	= SWT.COLOR_YELLOW;
				break;
		}
		
		return retColorCode;
	}
}
