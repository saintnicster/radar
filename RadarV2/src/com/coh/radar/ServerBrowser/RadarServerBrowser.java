package com.coh.radar.ServerBrowser;

import java.io.Closeable;
import java.io.IOException;

import com.apple.dnssd.BrowseListener;
import com.apple.dnssd.DNSSD;
import com.apple.dnssd.DNSSDException;
import com.apple.dnssd.DNSSDService;
import com.apple.dnssd.ResolveListener;
import com.coh.radar.Messages;

public class RadarServerBrowser  implements BrowseListener, Closeable {	
	public static String REG_TYPE = Messages.getString("BonjourServiceType"); //$NON-NLS-1$

	RadarServerBrowserController delegate;
	RadarServerList serverList;
	DNSSDService browser;
	
	static boolean bonjourIsInstalled = true;
	
	public static boolean isBonjourInstalled() {
		try {
			DNSSD.getNameForIfIndex(0);
		} catch (Throwable e) {
			bonjourIsInstalled = false;
			e.printStackTrace();
		}
		return bonjourIsInstalled;
	}
	public RadarServerBrowser(RadarServerBrowserController delegate) {
		this.delegate = delegate;
	}

	public void start() {
		this.serverList = new RadarServerList();

		if (isBonjourInstalled()) {
			try {
				browser = DNSSD.browse(REG_TYPE, this);
			} catch (DNSSDException e) {
				e.printStackTrace();
			} catch (UnsatisfiedLinkError e) {
				//System.out.println(e.getClass().getName());
				e.printStackTrace();
			}
		}
	}

	public void stop() {
		if (browser != null) {
			browser.stop();
		}

		this.serverList = null;
	}

	@Override
	public void close() throws IOException {
		stop();
	}

	@Override
	public void operationFailed(DNSSDService service, int errorCode) {
		System.out.println("Service:  "+service);
		System.out.println("Error Code:  "+errorCode);
	}

	@Override
	public void serviceFound(DNSSDService browser, int flags, int ifIndex,
			String serviceName, String regType, String domain) {
		RadarServerItem item = serverList.getOrAddServiceItem(serviceName, domain, ifIndex);

		DNSSDService resolver = null;
		try {
			resolver = DNSSD.resolve(flags, ifIndex, serviceName, regType, domain, (ResolveListener) item);
		} catch (DNSSDException e) {
			e.printStackTrace();
			if (resolver != null) {
				resolver.stop();
			}
		}

		if (flags != DNSSD.MORE_COMING) {
			delegate.updateServerList();
		}
	}

	@Override
	public void serviceLost(DNSSDService browser, int flags, int ifIndex,
			String serviceName, String regType, String domain) {
		synchronized(this) {
			serverList.removeServiceItemByName(serviceName, domain, ifIndex);

			if (flags != DNSSD.MORE_COMING) {
				delegate.updateServerList();
			}
		}
	}

	public RadarServerItem getServerItem(String serviceName) {
		RadarServerItem item = serverList.getServiceItemByName(serviceName);
		
		return item;
	}
}