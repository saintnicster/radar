package com.coh.radar.ServerBrowser;

import java.util.ArrayList;

public class RadarServerList extends ArrayList<RadarServerItem> {
	private static final long serialVersionUID = 392757123064120359L;
	
	public RadarServerItem getOrAddServiceItem(String serviceName, String domain, int ifIndex) {
		RadarServerItem returnedObject = getServiceItemByName(serviceName);
		if (returnedObject == null) {
			returnedObject = new RadarServerItem(serviceName, domain, ifIndex);
			
			add(returnedObject);
		}
		return returnedObject;
	}
	public RadarServerItem getServiceItemByName(String serviceName) {
		RadarServerItem returnedObject = null;
		for (RadarServerItem temp : this ) {
			if ( temp.serviceName.equals(serviceName) ) {
				returnedObject = temp;
				break;
			}
		}
		return returnedObject;//
	}
	
	public void removeServiceItemByName(String serviceName, String domain, int ifIndex) {
		RadarServerItem foundObject = null;
		for (RadarServerItem temp : this ) {
			if ( temp.serviceName.equals(serviceName) ) {
				foundObject = temp;
				break;
			}
		}
		
		if (foundObject != null) {
			remove(foundObject);
		}
	}
}
