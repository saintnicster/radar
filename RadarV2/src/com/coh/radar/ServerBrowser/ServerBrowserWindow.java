package com.coh.radar.ServerBrowser;

import java.util.ArrayList;
import java.util.Arrays;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.ProgressBar;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

import com.coh.radar.Messages;

public class ServerBrowserWindow extends Shell {
	ProgressBar progressBar;
	List lstServers;
	Button btnConnect;

	protected RadarServerBrowserController delegate;

	/**
	 * Create the shell.
	 * @param display
	 */
	public ServerBrowserWindow(Display display, RadarServerBrowserController delegate) {
		super(display, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);
		this.delegate = delegate;

		createContents();
	}

	/**
	 * Create the shell.
	 * @param display
	 * @wbp.parser.constructor
	 */
	public ServerBrowserWindow(Shell shell, RadarServerBrowserController delegate) {
		super(shell, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);
		this.delegate = delegate;

		createContents();
	}
	/**
	 * Create contents of the shell.
	 */
	private void createContents() {
		setLayout(new GridLayout(2, false));
		setSize(199, 225);

		Label lblScanning = new Label(this, SWT.NONE);
		lblScanning.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblScanning.setText(Messages.getString("ServerBrowserWindow.ScanningText")); //$NON-NLS-1$

		progressBar = new ProgressBar(this, SWT.INDETERMINATE);
		progressBar.setToolTipText(Messages.getString("ServerBrowserWindow.ScanningTooltip")); //$NON-NLS-1$
		GridData gd_progressBar = new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1);
		gd_progressBar.widthHint = 110;
		progressBar.setLayoutData(gd_progressBar);

		Label label = new Label(this, SWT.SEPARATOR | SWT.HORIZONTAL);
		label.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 2, 1));

		lstServers = new List(this, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
		lstServers.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				handleServerSelected();
			}
		});
		GridData gd_lstServers = new GridData(125, 125);
		gd_lstServers.horizontalSpan = 2;
		gd_lstServers.grabExcessVerticalSpace = true;
		gd_lstServers.horizontalAlignment = SWT.CENTER;
		gd_lstServers.grabExcessHorizontalSpace = true;
		lstServers.setLayoutData(gd_lstServers);

		btnConnect = new Button(this, SWT.NONE);
		btnConnect.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				handleConnectButton();
			}
		});
		btnConnect.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 2, 1));
		btnConnect.setText(Messages.getString("ServerBrowserWindow.btnConnectToServer")); //$NON-NLS-1$
		btnConnect.setEnabled(false);
	}

	protected void handleConnectButton() {
		if (lstServers.getSelectionCount() == 1) {
			delegate.connectToServer( (String) lstServers.getSelection()[0] );
			close();
		} else {
			btnConnect.setEnabled(false);
		}
	}
	protected void handleServerSelected( ) {
		if (lstServers.getSelectionCount() > 0) {
			btnConnect.setEnabled(true);
		} else {
			btnConnect.setEnabled(false);
		}
	}
	protected void refreshServerList(Object[] objects) {
		getDisplay().asyncExec( new ServerUpdate(objects, this) );
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}
}
class ServerUpdate implements Runnable {
	Object[] arrayToChange;
	ServerBrowserWindow shell;

	ServerUpdate(Object[] array, ServerBrowserWindow shell) {
		this.arrayToChange = array;
		this.shell = shell;
	}
	@Override
	public void run() {
		if (!shell.lstServers.isDisposed()) {
			String[] selection = null;
			if (shell.lstServers.getSelectionCount() > 0) {
				selection = shell.lstServers.getSelection();
			}
			shell.lstServers.removeAll();

			String[] strings = new String[arrayToChange.length];
			for (int i = 0; i < arrayToChange.length; i++) {
				strings[i] = (String) arrayToChange[i];
			}
			shell.lstServers.setItems(strings);

			if (selection != null) {
				ArrayList<String> selectedList = (ArrayList<String>) Arrays.asList(selection);
				int i = 0;
				for (Object temp : Arrays.asList(arrayToChange)) {
					if ( selectedList.contains(temp) ) {
						shell.lstServers.select(i);
					}
					i++;
				}
			}
		}
	}
}