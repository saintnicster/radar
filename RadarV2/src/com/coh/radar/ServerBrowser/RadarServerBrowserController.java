package com.coh.radar.ServerBrowser;

import java.net.InetSocketAddress;
import java.util.ArrayList;

import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import com.coh.radar.MainApplication;

public class RadarServerBrowserController implements Runnable {
	Display display;
	RadarServerBrowser browser;
	MainApplication delegate;
	ServerBrowserWindow window;
	
	public static boolean ComputerCanBrowseForOthers() {
		
		return RadarServerBrowser.isBonjourInstalled();
	}
	
	public RadarServerBrowserController(MainApplication delegate) {
		this.delegate = delegate;
	}
	@Override
	public void run() {
		startServer();
		
		window.open();
		window.layout();
		while ( !window.isDisposed() ) {
			if ( !display.readAndDispatch() ) {
				display.sleep();
			}
		}
		
		stopServer();
	}
	public void runServerBrowser(Shell parentShell) {
		window = new ServerBrowserWindow(parentShell, this);
		window.setImages(delegate.getImages());
		
		Rectangle parentBounds = parentShell.getBounds();
		Rectangle rect = window.getBounds();
		int x = parentBounds.x + (parentBounds.width - rect.width) / 2;
		int y = parentBounds.y + (parentBounds.height - rect.height) / 2;
		window.setLocation (x, y);
		
		
		display = this.window.getDisplay();
		display.asyncExec(this);
	}
	
	public void startServer() {
		stopServer();
		
		browser = new RadarServerBrowser(this);
		browser.start();
	}
	public void stopServer() {
		if (browser != null) {
			browser.stop();
		}
	}
	public void updateServerList() {
		synchronized (this) {
			ArrayList<String> serviceList = new ArrayList<String>();
			for ( RadarServerItem item : browser.serverList) {
				serviceList.add(item.serviceName);
			}
			window.refreshServerList(serviceList.toArray());
		}
	}
	public void connectToServer( String serviceName ) {
		InetSocketAddress address = null;
		
		RadarServerItem serverItem = browser.getServerItem(serviceName);
		if (serverItem != null) {
			address = serverItem.getAddressInfo();
		}
		delegate.connectToServer(address);
	}
	
}
