package com.coh.radar.ServerBrowser;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;

import com.apple.dnssd.DNSSD;
import com.apple.dnssd.DNSSDException;
import com.apple.dnssd.DNSSDService;
import com.apple.dnssd.QueryListener;
import com.apple.dnssd.ResolveListener;
import com.apple.dnssd.TXTRecord;

public class RadarServerItem implements ResolveListener, QueryListener {
	
	String serviceName;
	String domain;
	String hostName;
	int ifIndex;
	
	private InetAddress address = null;
	private int port = -1;
	
	public RadarServerItem(String serviceName, String domain, int ifIndex) {
		this.serviceName = serviceName;
		this.domain = domain;
		this.ifIndex = ifIndex;
	}
	
	public InetSocketAddress getAddressInfo() {
		InetSocketAddress socketAddress = null;
		if (address != null) {
			socketAddress = new InetSocketAddress(address, port);
		}
		return socketAddress;
	}

	public String getServiceName() {
		return this.serviceName;
	}

	@Override
	public void operationFailed(DNSSDService service, int errorCode) {
		System.out.println("operationFailed (resolve/query):  "+errorCode);
		System.out.println("Service:  "+service);
		service.stop();
	}

	@Override
	public void serviceResolved(DNSSDService resolver, int flags, int ifIndex,
			String fullName, String hostName, int port, TXTRecord txtRecord) {
		this.port = port;
		this.hostName = hostName;
		resolver.stop();
		
		DNSSDService query = null;
		try {
			query = DNSSD.queryRecord(flags, ifIndex, this.hostName, 1, 1, this);
		} catch (DNSSDException e) {
			e.printStackTrace();
			if (query != null) {
				query.stop();
			}
		}
	}

	@Override
	public void queryAnswered(DNSSDService query, int flags, int ifIndex,
			String fullName, int rrtype, int rrclass, byte[] rdata, int ttl) {
		address = null;
		try {
			address = InetAddress.getByAddress(rdata);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		query.stop();
	}
}
