package com.coh.radar;

import java.io.Closeable;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.prefs.Preferences;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;

import com.coh.radar.ServerBrowser.RadarServerBrowserController;
import com.swtdesigner.SWTResourceManager;

public class MainApplication implements Closeable {
	public boolean usePListFormatting = true;
	protected Display display;

	protected MapView mapView;
	protected TableView tableView;

	CityDemoProcessor cdp;
	RadarSocketServer server;
	RadarSocketClient client;
	RadarServerBrowserController browserController;

	private Image RADAR16_ICON, RADAR32_ICON , RADAR128_ICON;
	private Image[] radarIcons;
	private Preferences prefs = Preferences.userNodeForPackage(this.getClass());

	public boolean remoteEnabled, localEnabled, getRemote, programInitialized;
	private boolean processing = false;
	
	private RadarUpdater updater;
	
	public void stopUpdateChecker() {
		if (updater != null) {
			updater.stopUpdater();
			updater = null;
		}
	}
	
	public static void main(String[] args) {
		MainApplication window = null;
		try {
			window = new MainApplication();
			window.open();
		} catch (Throwable e) {
			e.printStackTrace();
		} finally {
			if (window != null) {
				window.close();
			}
		}
	}

	public void open() {
		Display.setAppName(RadarConsts.PROGRAM_NAME);
		Display.setAppVersion(RadarConsts.VERSION.toString());
		display = Display.getDefault();
		this.programInitialized = true;
		remoteEnabled = getPreference(RadarConsts.PREF_KEY_REMOTE, false);
		localEnabled  = getPreference(RadarConsts.PREF_KEY_LOCAL, true);
		getRemote     = getPreference(RadarConsts.PREF_KEY_GET_REMOTE, false);

		//Get a file remotely and distributing a file are mutually exclusive concepts.  
		//	Force this upon a restart
		if (remoteEnabled) {
			getRemote = false;
			setPreference(RadarConsts.PREF_KEY_GET_REMOTE, getRemote);
		}

		createContents();

		boolean runLoopRunning = true;
		while ( runLoopRunning ) {
			if (tableView.isDisposed()) {
				runLoopRunning = false;
				continue;
			}
			if ((localEnabled) && ((mapView == null) || (mapView.isDisposed()))) {
				runLoopRunning = false;
				continue;
			}
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}
	public void close() {
		if (tableView != null && !tableView.isDisposed()) {
			//tableView could be null if the program throws up on itself very early in the process
			tableView.close();
		}
		if (mapView != null && !mapView.isDisposed()) {
			mapView.close();
		}
		if (cdp != null) {
			cdp = null;
		}

		if (server != null && server.isServerRunning()) {
			try {
				server.stop();
			} catch (RadarException e) {
				e.printStackTrace();
			}
		}
		if (client != null) {
			try {
				client.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	public void bringToForeground() {
		if ( mapView != null && !mapView.isDisposed() ) {
			mapView.forceFocus();
		} else {
			if ( tableView != null && !tableView.isDisposed() ) {
				tableView.forceFocus();
			}
		}
	}
	public void generateWindows() {
		if (tableView != null && !tableView.isDisposed()) {
			tableView.close();
		}
		if (mapView != null && !mapView.isDisposed()) {
			mapView.close();
		}
		if (cdp != null) {
			cdp = null;
		}

		if (!localEnabled) {
			tableView = new TableView(display, this);
		} else {
			mapView = new MapView(display, this);
			tableView = new TableView(mapView, this);
		}
		
		if (this.programInitialized) {
			updater = new RadarUpdater(tableView);
			this.programInitialized = false;
		}

		if (getBoundsFromPref(RadarConsts.PREF_KEY_TABLE_WINDOW) == null) {
			initializeWindowPosition( );
		}

		updateServerUI();

		tableView.open();
		if (mapView != null && !mapView.isDisposed()) {
			mapView.open();
		}
	}

	private void createContents() {
		RADAR16_ICON  = SWTResourceManager.getImage(MainApplication.class, Messages.getString("Image.radarIcon16")); //$NON-NLS-1$
		RADAR32_ICON  = SWTResourceManager.getImage(MainApplication.class, Messages.getString("Image.radarIcon32")); //$NON-NLS-1$
		RADAR128_ICON = SWTResourceManager.getImage(MainApplication.class, Messages.getString("Image.radarIcon128")); //$NON-NLS-1$
		radarIcons    = new Image[] { RADAR16_ICON, RADAR32_ICON, RADAR128_ICON };

		RadarNotifyIcon icon = new RadarNotifyIcon(display.getSystemTray(), this);
		icon.makeTrayIcon(RADAR16_ICON);

		generateWindows();

		if (remoteEnabled) remoteEnable();
	}

	public Image[] getImages() {
		return radarIcons;
	}
	public void setLocalRemoteFlags( boolean local, boolean remote ) {
		if (remoteEnabled != remote) {
			remoteEnabled = remote;
			this.setPreference(RadarConsts.PREF_KEY_REMOTE, remoteEnabled);
		}

		if (localEnabled != local) {
			localEnabled = local;
			this.setPreference(RadarConsts.PREF_KEY_LOCAL, localEnabled);
		}
	}

	public void remoteEnable( ) {
		if (server == null) {
			try {
				server = new RadarSocketServer(this);
			} catch (RadarException e) {
				e.printStackTrace();
			}
		}
		if (!server.isServerRunning()) {
			try {
				server.start();
			} catch (RadarException e) {
				e.printStackTrace();
			}
		}
	}
	public void remoteDisable( ) {
		try {
			if (server != null && server.isServerRunning()) {
				server.stop();

				server = null;
				updateServerUI();
			}
		} catch (RadarException e) {
			e.printStackTrace();
		}
	}

	public void processFile() {
		processFile( getPreference(RadarConsts.PREF_KEY_FILENAME) );
	}
	private void processFile( String filename ) {
		if (processing && !getRemote) {
			return;
		}
		String fileToProcess = filename;
		
		if (cdp != null) cdp = null;
		tableView.handleLocalDisplay(null);
		refreshMap();
		
		if (getRemote && !processing) {
			processing = true;
			
			if (client == null) {
				InetSocketAddress serverAddress = tableView.getNetInformation();
				client = new RadarSocketClient(serverAddress);
			}
			new Thread( new Runnable() {
				@Override
				public void run() {
					// TODO Auto-generated method stub
					String fileToProcess = Messages.getString("Empty");  //$NON-NLS-1$
					
					try {
						client.getFileFromServer();
					} catch (RadarException e) {
						displayAlert( e.getMessage() );
						processing = false;
						
						if (checkForBrowser()) {
							MainApplication.this.startScanner();
						}
						return;
					}

					synchronized (this) {
						while (client.gettingFile) {
							//Do nothing for a while
							try {
								wait(50);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
						}
					}

					if (client.success) {
						fileToProcess = client.filename;
					} else {
						fileToProcess = Messages.getString("Empty");  //$NON-NLS-1$
					}
					
					MainApplication.this.processFile(fileToProcess);
				}
			}).start();
			
			fileToProcess = Messages.getString("Empty");  //$NON-NLS-1$
			return;
		}
		processing = true;
		
		if ((remoteEnabled && usePListFormatting) || localEnabled || getRemote) {

			cdp = new CityDemoProcessor(fileToProcess);
			try {
				cdp.processFile();
			} catch (RadarException e) {
				e.printStackTrace();
				displayAlert( e.getMessage() );
			}
			
			if (localEnabled || getRemote) {
				tableView.handleLocalDisplay( cdp.entityMap.getEntityList() );
	
				refreshMap();
			}
		}
		if (remoteEnabled && (server != null && server.isServerRunning())) {
			try {
				server.pushFileToConnections();
			} catch (RadarException e) {
				e.printStackTrace();
			}
		}

		processing = false;
	}
	public void highlightEntityOnMap( String objectID ) {
		if (cdp != null) {
			//cdp.entityMap.getEntityList(), cdp.getMinBound(), cdp.getMaxBound()
			mapView.addReticleOnMap(cdp.entityMap.getEntity(objectID), cdp.getMinBound(), cdp.getMaxBound());
		}
	}
	public void refreshMap( ) {
		display.asyncExec( new MapRefresher(mapView, cdp));
	}

	public void updateServerUI() {
		Boolean bonjour = null;
		InetSocketAddress address = null;
		String serviceName = Messages.getString("Empty");  //$NON-NLS-1$
		if (remoteEnabled && server != null && server.isServerRunning() ) {
			address = server.getAddressInfo();
			bonjour = new Boolean(server.bonjourIsDisabled());
			serviceName = server.getServiceName();
		}
		updateServerUI(address, bonjour);
		
		String titleBar = RadarConsts.PROGRAM_NAME;
		if (!serviceName.equals(Messages.getString("Empty"))) {  //$NON-NLS-1$
			titleBar = titleBar.concat(" ("+serviceName+")");   //$NON-NLS-1$//$NON-NLS-2$
		}
		tableView.updateTitleBar(titleBar);
	}
	private void updateServerUI( InetSocketAddress socketAddress, Boolean bonjourDisabled ) {
		InetSocketAddress address = socketAddress;

		tableView.updateNetworkData(address, bonjourDisabled);
	}

	public String getFileToServe( ) {
		return getPreference(RadarConsts.PREF_KEY_FILENAME);
	}
	public void setPreference( String key, boolean value ) {
		prefs.putBoolean(key, value);
	}
	public void setPreference( String key, String value ) {
		prefs.put(key, value);
	}

	public String  getPreference ( String key ) {
		return getPreference(key, Messages.getString("Empty"));  //$NON-NLS-1$
	}
	private boolean getPreference( String key, boolean def ) {
		return prefs.getBoolean(key, def);
	}
	private String getPreference ( String key, String def ) {
		return prefs.get(key, def);
	}

	public void saveBoundsAsPref( String key, Rectangle bounds) {
		if (bounds != null) {
			String value = Messages.getString("Empty")+bounds.x+Messages.getString("MainApplication.BoundPrefSeparator")+bounds.y+Messages.getString("MainApplication.BoundPrefSeparator")+bounds.width+Messages.getString("MainApplication.BoundPrefSeparator")+bounds.height; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
			setPreference(key, value);
		}
	}
	public Rectangle getBoundsFromPref( String key ) {
		String[] tokens = getPreference(key).split(Messages.getString("MainApplication.BoundPrefSeparator")); //$NON-NLS-1$
		Rectangle bounds = null;
		if (tokens.length == 4) {
			try {
				bounds = new Rectangle(new Integer(tokens[0]), new Integer(tokens[1]),
						new Integer(tokens[2]), new Integer(tokens[3]) );
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
		}
		return bounds;
	}

	public void initializeWindowPosition( ) {
		tableView.resetDisplay();

		Rectangle mainWindow = display.getPrimaryMonitor().getClientArea();
		Rectangle tableBounds = tableView.getBounds();

		tableBounds.x = mainWindow.x;
		tableBounds.y = mainWindow.y;

		tableView.setBounds(tableBounds);
		saveBoundsAsPref(RadarConsts.PREF_KEY_TABLE_WINDOW, tableBounds);

		if (mapView != null && !mapView.isDisposed()) {
			mapView.resetMap();
			Rectangle mapBounds = mapView.getBounds();
			if (mapBounds != null) {
				mapBounds.x      = mainWindow.x + tableBounds.width;
				mapBounds.y      = mainWindow.y;
				mapBounds.width  = mainWindow.width - tableBounds.width;
				mapBounds.height = mainWindow.height;

				mapView.setBounds(mapBounds);
				saveBoundsAsPref(RadarConsts.PREF_KEY_MAP_WINDOW, mapBounds);
			}
		}
	}
	public void startScanner() {display.asyncExec(
		new Runnable() {
			@Override
			public void run() {
				browserController = new RadarServerBrowserController(MainApplication.this);
				browserController.runServerBrowser(tableView);
			}
		});
	}
	public void connectToServer(InetSocketAddress address) {
		tableView.updateNetworkData(address, null);

		client = new RadarSocketClient(address);
	}
	public boolean checkForBrowser() {
		return RadarServerBrowserController.ComputerCanBrowseForOthers();
	}

	public void displayAlert(final String alertText) {
		display.asyncExec(
			new Runnable() {
				@Override
				public void run() {
					MessageBox box = new MessageBox(tableView, SWT.ICON_ERROR | SWT.OK );
					box.setText(Messages.getString("MainApplication.ErrorMessageBoxTitlePrefix")+RadarConsts.PROGRAM_NAME); //$NON-NLS-1$
					box.setMessage(alertText);
					box.open();
				}
			});
	}
}

class MapRefresher implements Runnable {
	MapView _mapView = null;
	EntityMap _entityMap = null;
	CityDemoProcessor _cdp;
	public MapRefresher(MapView mapView, CityDemoProcessor cdp) {
		this._mapView = mapView;
		this._cdp = cdp;
	}
	
	@Override
	public void run() {
		if (this._mapView != null && !this._mapView.isDisposed()) {
			if (this._cdp != null && this._cdp.entityMap != null && this._cdp.entityMap.getPlayerObject() != null ) {
				this._mapView.setZMidPoint((int) this._cdp.entityMap.getPlayerObject().posZAxis);
				this._mapView.handleMapDisplay(this._cdp.entityMap.getEntityList(),this._cdp.getMinBound(), this._cdp.getMaxBound());
				//this._mapView.handleMapDisplaySubviews(this._cdp.entityMap.getEntityList(),this._cdp.getMinBound(), this._cdp.getMaxBound());
			} else {
				this._mapView.clearPaintListeners();
			}
			this._mapView.redraw();
		}
	}
}
