package com.coh.radar;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;

import com.coh.radar.Entity.EntityType;
import com.swtdesigner.SWTResourceManager;

public class MapView extends Shell {
	protected MainApplication delegate = null;
	private FloatXY scalers = null;
	private int zMidPoint = 0;
	
	private EntityPainter reticle = null;
	
	public void updateSizeDisplay() {
		synchronized (this) {
			Rectangle bounds = getBounds();
			setText(RadarConsts.PROGRAM_NAME+" // ("+bounds.width+" x "+bounds.height+")");
			redraw();
		}
	}
	
	@Override
	public void open() {
		Rectangle tempBounds = delegate.getBoundsFromPref(RadarConsts.PREF_KEY_MAP_WINDOW);
		if (tempBounds != null) setBounds(tempBounds);
		
		super.open();
	}
	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

	/**
	 * @wbp.parser.constructor
	 */
	public MapView(Display display, MainApplication delegate) {
		super(display, SWT.CLOSE | SWT.MIN | SWT.RESIZE);		
		this.delegate = delegate;
		
		createContents();
	}

	/**
	 * Create contents of the shell.
	 */
	protected void createContents() {
		setBackground(SWTResourceManager.getColor(SWT.COLOR_BLACK));
		
		addDisposeListener(new DisposeListener() {
			@Override
			public void widgetDisposed(DisposeEvent e) {
				delegate.saveBoundsAsPref(RadarConsts.PREF_KEY_MAP_WINDOW, MapView.this.getBounds());
			}
		});
		addListener(SWT.Resize, new Listener() {
			@Override
			public void handleEvent(Event event) {
				delegate.refreshMap();
				
				updateSizeDisplay();
			}
		});

		setText(RadarConsts.PROGRAM_NAME);
		setSize(500, 500);
		
		if (delegate != null) {
			setImages(delegate.getImages());
		}
	}
	public void addReticleOnMap(Entity entityToHighlight, Point minBound, Point maxBound) {
		if (reticle != null) {
			removePaintListener(reticle);
			reticle = null;
		}
		
		reticle = new EntityPainter(entityToHighlight, minBound, maxBound, this);
		reticle.setReticleFlag(true);
		
		addPaintListener(reticle);
		redraw();
	}
	public void resetMap( ) {
		clearPaintListeners();
	}
	protected void clearPaintListeners( ) {
		if (reticle != null) {
			removePaintListener(reticle);
			reticle = null;
		}
		
		ArrayList<Listener> list = new ArrayList<Listener>(Arrays.asList(this.getListeners(SWT.Paint)));
		
		for (Listener item : list) {
			this.removeListener(SWT.Paint, item);
		}
	}
	
	public void setScalers(Point minPoint, Point maxPoint) {
		int	demoWidth	= 1, demoHeight	= 1;
		
		Rectangle bounds = getBounds();
		
		demoWidth 	= (maxPoint.x - minPoint.x) + (RadarConsts.WINDOW_BUFFER * 2);
		demoHeight	= (maxPoint.y - minPoint.y) + (RadarConsts.WINDOW_BUFFER * 2);
		
		this.scalers = new FloatXY((float) bounds.width / demoWidth,
                                   (float) bounds.height / demoHeight);
		
		System.out.println("Demo Size: "+demoWidth+"x"+demoHeight);
		System.out.println("Demo Scalers:  "+this.scalers);
	}
	public FloatXY getScalers() {
		return this.scalers;
	}
	
	public void handleMapDisplay(ArrayList<Entity> entityList, Point minBound, Point maxBound) {
		EntityPainter playerIcon = null;
		
		@SuppressWarnings("unchecked")
		ArrayList<Entity> reverseList = (ArrayList<Entity>) entityList.clone();
		
		Collections.reverse(reverseList);
		
		clearPaintListeners(); //remove previous plottings
		setScalers(minBound, maxBound);
		for (Entity entity : reverseList) {
			if (entity.entityName.equals(Messages.getString("Empty"))) { //$NON-NLS-1$
				continue;
			}
			if (entity.entityType == EntityType.PLAYER) {
				playerIcon = new EntityPainter(entity, minBound, maxBound, this);
			} else {
				addPaintListener(new EntityPainter(entity, minBound, maxBound, this));
			}
		}
		
		if (playerIcon != null) {
			addPaintListener(playerIcon);
		}
	}
	
	/**
	 * @param zMidPoint the zMidPoint to set
	 */
	public void setZMidPoint(int zMidPoint) {
		this.zMidPoint = zMidPoint;
	}
	/**
	 * @return the zMidPoint
	 */
	public int getZMidPoint() {
		return zMidPoint;
	}
}
class EntityPainter implements PaintListener {
	protected Entity entityToPaint;
	MapView parent;
	Point demoMinBound, demoMaxBound;
	boolean isReticle = false;
	
	public EntityPainter(Entity entity, Point demoMinBound, Point demoMaxBound, MapView parent ) {
		this.entityToPaint = entity;
		this.demoMinBound = demoMinBound;
		this.demoMaxBound = demoMaxBound;
		
		this.parent = parent;
	}

	@Override
 	public void paintControl(PaintEvent event) {
		if (isReticle) {
			paintReticle(event);
		} else {
			paintMarker(event);
		}
	}
	
	public void setReticleFlag(boolean flag) {
		isReticle = flag;
	}
	private void paintReticle( PaintEvent event ) {
		int x = 0, y = 0;
		float xF = 0.0F, yF = 0.0F;
		
		xF = RadarConsts.WINDOW_BUFFER + demoMaxBound.x - entityToPaint.posXAxis;
		yF = entityToPaint.posYAxis - (demoMinBound.y - RadarConsts.WINDOW_BUFFER);

		FloatXY scaler = parent.getScalers();
		x = (int) (xF * scaler.x);
		y = (int) (yF * scaler.y);

		event.gc.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		
		event.gc.drawRectangle( x-(RadarConsts.POINT_RADIUS*2), y-(RadarConsts.POINT_RADIUS*2), (RadarConsts.POINT_RADIUS*4)-1, (RadarConsts.POINT_RADIUS*4) );
		
		event.gc.drawLine( x-(RadarConsts.POINT_RADIUS*3), y, x+(RadarConsts.POINT_RADIUS*3)-1, y );
		event.gc.drawLine( x, y-(RadarConsts.POINT_RADIUS*3), x, y+(RadarConsts.POINT_RADIUS*3)-1 );
	}
	
	private void paintMarker(PaintEvent event) {
		int x, y, bgColorCode;
		float xF, yF;

		switch (entityToPaint.entityType) {
			case PLAYER:
				bgColorCode = SWT.COLOR_GREEN;
				break;
			case TEAMMATE:
				bgColorCode = SWT.COLOR_DARK_CYAN;
				break;
			case GLOWIE:
				bgColorCode = SWT.COLOR_YELLOW;
				break;
			case DOOR:
				bgColorCode = SWT.COLOR_BLUE;
				break;
			case HOSTAGE:
				bgColorCode = SWT.COLOR_RED;
				break;
			case CAPTIVE:
				bgColorCode = SWT.COLOR_RED;
				break;
			case ANTIGLOWIE:
				bgColorCode = SWT.COLOR_YELLOW;
				break;
			case QUANTUM:
				bgColorCode = SWT.COLOR_DARK_MAGENTA;
				break;
			case DESTRUCT:
				bgColorCode	= SWT.COLOR_YELLOW;
				break;
			case CONTACT_HAS_MISH:
				bgColorCode = SWT.COLOR_DARK_RED;
				break;
			case CONTACT_INPROG:
				bgColorCode = SWT.COLOR_DARK_RED;
				break;
			case CONTACT_MISSIONOVER:
				bgColorCode = SWT.COLOR_DARK_RED;
				break;
			default:
				bgColorCode = SWT.COLOR_DARK_GRAY;
				break;
		}

		event.gc.setBackground( SWTResourceManager.getColor(bgColorCode) );
		event.gc.setForeground( SWTResourceManager.getColor(SWT.COLOR_WHITE) );
		
		xF = RadarConsts.WINDOW_BUFFER + demoMaxBound.x - entityToPaint.posXAxis;
		yF = entityToPaint.posYAxis - (demoMinBound.y - RadarConsts.WINDOW_BUFFER);
		
		FloatXY scaler = parent.getScalers();
		x = (int) (xF * scaler.x);
		y = (int) (yF * scaler.y);
		
		int zMidPoint = parent.getZMidPoint();
		
		if (entityToPaint.posZAxis > (zMidPoint + 3)) {
			//Triangle pointing up
			int[] pointArray = { x,	(y-RadarConsts.POINT_RADIUS), 
					(x-RadarConsts.POINT_RADIUS),	(y+RadarConsts.POINT_RADIUS),
					(x+RadarConsts.POINT_RADIUS), (y+RadarConsts.POINT_RADIUS) };
			event.gc.fillPolygon(pointArray);
		} else if (entityToPaint.posZAxis < (zMidPoint - 3)) {
			//triangle pointing down
			int[] pointArray = { x, (y+RadarConsts.POINT_RADIUS),
					(x-RadarConsts.POINT_RADIUS),	(y-RadarConsts.POINT_RADIUS),
					(x+RadarConsts.POINT_RADIUS), 	(y-RadarConsts.POINT_RADIUS) };
			event.gc.fillPolygon(pointArray);
		} else {
			event.gc.fillOval((x-RadarConsts.POINT_RADIUS),	(y-RadarConsts.POINT_RADIUS), 
					(RadarConsts.POINT_RADIUS*2), (RadarConsts.POINT_RADIUS*2));
		}
	}
}