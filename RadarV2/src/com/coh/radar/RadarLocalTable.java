package com.coh.radar;

import java.util.ArrayList;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Table;


public class RadarLocalTable extends Table {
	MainApplication delegate;
	
	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}
	
	RadarLocalTable(Composite parent, MainApplication delegate) {
		super( parent, SWT.SINGLE | SWT.BORDER | SWT.FULL_SELECTION | SWT.VIRTUAL );
		this.delegate = delegate;
		
		createColumns( );
		
		addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (((e.item).getClass()).equals(TableItem.class)) {
					(RadarLocalTable.this.delegate).highlightEntityOnMap(((TableItem) e.item).getText(0));
				}
			}
		});
	}
	
	public void resetTable( ) {
		removeAll();
		packAllColumns( );
	}
	
	private void createColumns( ) {
		setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		setLinesVisible(true);
		setHeaderVisible(true);

		TableColumn tableColumn = new TableColumn( this, SWT.NONE );
		tableColumn.setWidth(30);
		tableColumn.setText(Messages.getString("RadarLocalTable.IDColumnHead")); //$NON-NLS-1$

		TableColumn tableColumn_1 = new TableColumn(this, SWT.NONE);
		tableColumn_1.setWidth(150);
		tableColumn_1.setText(Messages.getString("RadarLocalTable.NameColumnHead")); //$NON-NLS-1$

		TableColumn tableColumn_2 = new TableColumn(this, SWT.NONE);
		tableColumn_2.setWidth(70);
		tableColumn_2.setText(Messages.getString("RadarLocalTable.TypeColumnHead")); //$NON-NLS-1$

		TableColumn tableColumn_3 = new TableColumn(this, SWT.NONE);
		tableColumn_3.setWidth(25);
		tableColumn_3.setText(Messages.getString("RadarLocalTable.XColumnHead")); //$NON-NLS-1$

		TableColumn tableColumn_4 = new TableColumn(this, SWT.NONE);
		tableColumn_4.setWidth(25);
		tableColumn_4.setText(Messages.getString("RadarLocalTable.YColumnHead")); //$NON-NLS-1$

		TableColumn tableColumn_5 = new TableColumn(this, SWT.NONE);
		tableColumn_5.setWidth(25);
		tableColumn_5.setText(Messages.getString("RadarLocalTable.ZColumnHead")); //$NON-NLS-1$
		
		packAllColumns();
	}
	
	public void packAllColumns( ) {
		TableColumn[] columns = this.getColumns();
		for (int i = 0; i < columns.length; i++) {
			if (i != 1) {
				columns[i].pack();
			}
		}
	}
	
	public int populateTable( ArrayList<Entity> entityList ) {
		TableItem row;
		int counter = 0;
		
		for (Entity entity : entityList)
		{
			if (entity.entityName.equals(Messages.getString("Empty"))) //$NON-NLS-1$
			{
				continue;
			}
			
			row = new TableItem (this, SWT.NONE);
			row.setText(0, entity.objectID );
			row.setText(1, entity.entityName );
			row.setText(2, entity.getEntityTypeText() );
			row.setText(3, new Integer( ((int) entity.posXAxis)).toString() );
			row.setText(4, new Integer( ((int) entity.posYAxis)).toString() );
			row.setText(5, new Integer( ((int) entity.posZAxis)).toString() );
			
			counter++;
		}
		packAllColumns();
		
		return counter;
	}
	
}
