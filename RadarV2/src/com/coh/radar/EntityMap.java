package com.coh.radar;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import com.coh.radar.Entity.EntityType;
import com.dd.plist.*;


public class EntityMap extends HashMap<String, Entity> {
	private static final long serialVersionUID = 8556113014752119600L;
	
	private String playerObjectID = Messages.getString("Empty"); //$NON-NLS-1$
	
	public void setPlayerObjectID(String playerObjectID) {
		this.playerObjectID = playerObjectID;
	}
	public Entity getPlayerObject() {
		Entity returnedEntity = null;
		if (! this.playerObjectID.equals(Messages.getString("Empty"))) { //$NON-NLS-1$
			returnedEntity = getEntity(this.playerObjectID);
		}
		return returnedEntity;
	}
	
	public Entity getEntity( String objectID ) {
		Entity output = null;
		
		output = this.get(objectID);
		if (output == null) {
			output = new Entity(objectID);
			this.put(objectID, output);
		}
		return output;
	}
	public ArrayList<Entity> getEntityList()
	{
		ArrayList<Entity> returnList = new ArrayList<Entity>();
		Object[] entityKeys = this.keySet().toArray();
		
		Arrays.sort(entityKeys, new KeyComparer(this));
		
		ArrayList<Object> keys = new ArrayList<Object>(Arrays.asList(entityKeys));
		for(Object key: keys) {
			returnList.add(this.get(key));			
		}
		
		return returnList;
	}
	
	public static EntityMap buildMapFromPList( String filename ) throws RadarException {
		File plistFile = new File(filename);
		if (!plistFile.exists() || !plistFile.canRead()) {
			throw new RadarException("Cannot open file - "+filename, null);
		}
		NSArray arrayOfItems = null;
		try {
			arrayOfItems = (NSArray) PropertyListParser.parse(plistFile);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		List<NSObject> list = Arrays.asList(arrayOfItems.getArray());
		
		EntityMap returnedMap = new EntityMap();
		
		for (NSObject nsObject : list) {
			if (nsObject.getClass() == NSDictionary.class) {
				NSDictionary nsDict = (NSDictionary) nsObject;
				String objID = nsDict.objectForKey(Messages.getString("Entity.plistKeyObjID")).toString(); //$NON-NLS-1$
				
				Entity tempEntity = returnedMap.getEntity(objID);
				tempEntity.mapFromNSDictionary(nsDict);
				
				if (tempEntity.entityType == EntityType.PLAYER) { 
					returnedMap.setPlayerObjectID(objID); 
				}

			}
		}
		
		return returnedMap;
	}
	
	public String getPListXML() {
		NSArray entityArray = new NSArray(this.size());
		
		int i = 0;
		for (Entity tmpEntity : this.getEntityList()) {
			entityArray.setValue(i, tmpEntity.toNSDictionary());
			i++;
		}
		
		return entityArray.toXMLDocument();
		//return entityArray.toXMLPropertyList();
	}
}

class KeyComparer implements Comparator<Object> {
	EntityMap entityMap;
	
	KeyComparer(EntityMap list) {
		this.entityMap = list;
	}
	
	@Override
	public int compare(Object arg0, Object arg1) {
		Entity e1 = entityMap.get(arg0);
		Entity e2 = entityMap.get(arg1);

		//Entity.compareTo now makes the comparison against Type, then Name, then Object ID
		return (e1.compareTo(e2));
	}
}
