package com.coh.radar;

import java.io.BufferedOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.ConnectException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class RadarSocketClient implements Runnable, Closeable {
	InetSocketAddress address;
	ServerSocket server;
	Socket outbound;
	String filename;

	BufferedOutputStream bos = null;
	FileOutputStream fos = null;

	boolean gettingFile = false;
	boolean success = false;

	public RadarSocketClient(InetSocketAddress address) {
		this.address = address;
	}

	@Override
	public void close() throws IOException {
		clearFile();

		if (outbound != null && !outbound.isClosed()) {
			try {
				outbound.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if (server != null && !server.isClosed()) {
			try {
				server.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	public void clearFile() {
		if (filename == null || filename.equals(Messages.getString("Empty"))) { //$NON-NLS-1$
			return;
		}

		File fileToDelete = new File(filename);
		if (fileToDelete.exists() && fileToDelete.canWrite()) {
			System.out.println("Deleting "+filename+" - "+(!fileToDelete.exists() || fileToDelete.delete())); //$NON-NLS-1$ //$NON-NLS-2$
		} else {
			System.out.println(Messages.getString("RadarSocketClient.ErrorDeletingFile")+filename); //$NON-NLS-1$
		}
	}

	/* Example code for serving the file over the socket was found at 
	 * 
	 *	http://edinkysoft.appspot.com/jsp/articles/javafilesocket.html
	 */
	@Override
	public void run() {
		if (outbound == null || !outbound.isConnected() || outbound.isClosed()) {
			gettingFile = false;
			success = false;
			return;
		}
		byte[] fileInfo = new byte[50];
		try {
			outbound.getInputStream().read(fileInfo);
			String roundTrip = new String(fileInfo, Messages.getString("UTF8Charset")); //$NON-NLS-1$
			String newFileName = roundTrip.split(Messages.getString("MessageFileInfoSeperator"))[0]; //$NON-NLS-1$

			int size = Integer.parseInt(getFileSize(roundTrip.split(Messages.getString("MessageFileInfoSeperator"))[1])); //$NON-NLS-1$
			byte[] fileByteArray = new byte[size];

			File file = new File(System.getProperty("java.io.tmpdir")+newFileName); //$NON-NLS-1$
			filename = file.getAbsolutePath();

			int dataSize = 0;
			if ( file.createNewFile() || file.canWrite() ) {
				fos = new FileOutputStream(file);
				bos = new BufferedOutputStream(fos);

				while ((dataSize = outbound.getInputStream().read(fileByteArray, 0, fileByteArray.length)) > 0) {
					bos.write(fileByteArray, 0, dataSize);
				}
			}
			success = true;
		} catch (IOException e) {
			e.printStackTrace();
			success = false;
		} finally {
			if (bos != null) { try { bos.flush(); bos.close(); } catch (IOException e) {e.printStackTrace();} }
			if (fos != null) { try { fos.flush(); fos.close(); } catch (IOException e) {e.printStackTrace();} }
		}

		gettingFile = false;
	}
	private String getFileSize(String str) {
		int strlength = str.length();
		int endpos = 0;
		for(int i=0; i < strlength; i++) {
			char digit = str.charAt(i);
			if(digit >= '0' && digit <= '9') {
				continue;
			}
			endpos = i;
			break;
		}
		return str.substring(0,endpos);
	}

	public void getFileFromServer() throws RadarException {
		if (this.address == null || this.address.isUnresolved()) {
			throw new RadarException(Messages.getString("RadarSocketClient.ErrorInvalidAddress"), null); //$NON-NLS-1$
		}
		gettingFile = true;

		clearFile();

		try {
			outbound = new Socket(this.address.getAddress(), this.address.getPort());
		} catch (ConnectException e) {
			e.printStackTrace();
			gettingFile = false;
			throw new RadarException(Messages.getString("RadarSocketClient.ErrorConnectException"), e); //$NON-NLS-1$
		} catch (IOException e) {
			e.printStackTrace();
			gettingFile = false;
			throw new RadarException(Messages.getString("RadarSocketClient.ErrorIOException"), e); //$NON-NLS-1$
		} catch (SecurityException e) {
			e.printStackTrace();
			gettingFile = false;
			throw new RadarException(Messages.getString("RadarSocketClient.ErrorSecurityManager")+e.getLocalizedMessage(), e); //$NON-NLS-1$
		}

		new Thread(this).start();
	}
}
