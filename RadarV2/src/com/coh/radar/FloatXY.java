package com.coh.radar;

class FloatXY {
	public float x, y;
	
	public FloatXY(float x, float y) {
		this.x = x;
		this.y = y;
	}
	
	public String toString() {
		return Messages.getString("FloatXY.FloatXY_toString"). //$NON-NLS-1$
				replaceAll(Messages.getString("VariableOne"), Messages.getString("Empty")+x). //$NON-NLS-1$ //$NON-NLS-2$
				replaceAll(Messages.getString("VariableTwo"), Messages.getString("Empty")+y); //$NON-NLS-1$ //$NON-NLS-2$
	}
}