package com.coh.radar;

public class RadarConsts {
	
	/* Preference Keys */
	public static String PREF_KEY_REMOTE       = "REMOTE_ENABLED"; //$NON-NLS-1$
	public static String PREF_KEY_LOCAL        = "LOCAL_ENABLED"; //$NON-NLS-1$
	public static String PREF_KEY_FILENAME 	   = "DEFAULT_FILENAME"; //$NON-NLS-1$
	public static String PREF_KEY_MAP_WINDOW   = "MAP_RECT"; //$NON-NLS-1$
	public static String PREF_KEY_TABLE_WINDOW = "TABLE_RECT"; //$NON-NLS-1$
	public static String PREF_KEY_GET_REMOTE   = "GET_REMOTE"; //$NON-NLS-1$
	
	/* Program Name and Version Number */
	public static String PROGRAM_NAME = Messages.getString("RadarConsts.ProgramName"); //$NON-NLS-1$
	public static Version VERSION = new Version(0,7,1,0);
	
	/* width used for the icons */
	public static int POINT_RADIUS = 4;
	
	/* Arbitrary padding for around the map */
	public static int WINDOW_BUFFER = 300;
}
