package com.coh.radar;

import java.io.File;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.Arrays;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Composite;

import com.swtdesigner.SWTResourceManager;

public class TableView extends Shell {
	private Composite conditionalUI;
	private Combo cmbLocSelector;
	private Group grpNetworkInfo;
	private Label lblStatusBar, bonjourIcon;
	private RadarLocalTable table;
	private Text txtFilename, txtHostname, txtPort;
	private Button btnGetFileElsewhere, btnProcessFile, btnSelectFile, btnScan;
	MainApplication delegate = null;

	private String hostnameText = Messages.getString("TableView.DefaultIPDisplay"), portText = Messages.getString("TableView.DefaultPortDisplay"); //$NON-NLS-1$ //$NON-NLS-2$
	private int previousSelection = -1;

	@Override
	public void open() {
		Rectangle tempBounds = delegate.getBoundsFromPref(RadarConsts.PREF_KEY_TABLE_WINDOW);
		if (tempBounds != null) setBounds(tempBounds);

		super.open();
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

	/**
	 * Create the shell.
	 * @param display
	 * @wbp.parser.constructor
	 */
	public TableView(Display display, MainApplication delegate) {
		super(display, SWT.MIN | SWT.CLOSE | SWT.RESIZE | SWT.TITLE);
		this.delegate = delegate;

		makeUI();
	}
	/**
	 * Create the shell.
	 * @param display
	 * @wbp.parser.constructor
	 */
	public TableView(Shell shell, MainApplication delegate) {
 		super(shell, SWT.CLOSE | SWT.RESIZE | SWT.TITLE);
		this.delegate = delegate;

		makeUI();
	}
	public void updateTitleBar(final String titleBar) {
		getDisplay().asyncExec(new Runnable() {
			@Override
			public void run() {
				TableView.this.setText(titleBar);
			}
		});
	}
	public void updateStatusBar(String message) {
		lblStatusBar.setText(message);
	}
	public void updateNetworkData(InetSocketAddress addressInfo, Boolean bonjourDisabled) {
		final InetSocketAddress address = addressInfo;
		final Boolean bonjour = bonjourDisabled;
		
		getDisplay().asyncExec( new Runnable() {
			@Override
			public void run() {
				if (address != null) {
					hostnameText = (address.getAddress().getHostAddress()).toString();
					portText = new Integer(address.getPort()).toString();
				} else {
					hostnameText = Messages.getString("TableView.DefaultIPDisplay");  //$NON-NLS-1$
					portText = Messages.getString("TableView.DefaultPortDisplay"); //$NON-NLS-1$
				}
				if (txtHostname != null && !txtHostname.isDisposed()) {
					txtHostname.setText(hostnameText);
				}
				if (txtPort != null && !txtPort.isDisposed()) {
					txtPort.setText(portText);
				}
				
				if (bonjour != null) {
					setBonjourDisabled(bonjour);
				}
			}
		});
	}
	public InetSocketAddress getNetInformation() {
		InetSocketAddress address = null;
		
		try {
			address = new InetSocketAddress(txtHostname.getText(), Integer.parseInt(txtPort.getText()));
		} catch (NumberFormatException e) {
			//e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}
		
		return address;
	}
	public void setTableItemCounter(int count) {
		String statusText = Messages.getString("TableView.statusbarEntityCount"); //$NON-NLS-1$
		statusText = statusText.replace(Messages.getString("TableView.statusbarEntityCountNumber"), new Integer(count).toString()); //$NON-NLS-1$

		updateStatusBar(statusText);
	}
	public void handleLocalDisplay( final ArrayList<Entity> entityList ) {
		getDisplay().asyncExec(new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				setTableItemCounter(0);
				resetDisplay();
				
				if (entityList != null) {
					int count = table.populateTable( entityList );
					if (count != 0) {
						setTableItemCounter(count);
					}
				}
			}
		});
	}

	private void makeUI( ) {
		setLayout(new GridLayout(4, false));

		Label lblDemoFile = new Label(this, SWT.NONE);
		lblDemoFile.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblDemoFile.setText(Messages.getString("TableView.lblDemoFile")); //$NON-NLS-1$

		txtFilename = new Text(this, SWT.BORDER);
		txtFilename.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		txtFilename.setText(delegate.getPreference(RadarConsts.PREF_KEY_FILENAME));

		btnSelectFile = new Button(this, SWT.NONE);
		btnSelectFile.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				handleFileSelectButton();
			}
		});
		btnSelectFile.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		btnSelectFile.setText(Messages.getString("TableView.lblSelectFile")); //$NON-NLS-1$

		Label lblDisplayFile = new Label(this, SWT.NONE);
		lblDisplayFile.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblDisplayFile.setText(Messages.getString("TableView.lblDisplayFile")); //$NON-NLS-1$

		cmbLocSelector = new Combo(this, SWT.READ_ONLY);
		cmbLocSelector.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				handleLocSelector( );
			}
		});
		cmbLocSelector.setItems(new String[] {Messages.getString("TableView.LocationLocal"), Messages.getString("TableView.LocationRemote"), Messages.getString("TableView.LocationBoth")}); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		int selectionValue = -1;
		if (delegate.remoteEnabled && delegate.localEnabled) {
			selectionValue = 2;
		} else {
			if (delegate.remoteEnabled) {
				selectionValue = 1;
			} else if (delegate.localEnabled) {
				selectionValue = 0;
			} else {
				selectionValue = 2;
			}
		}
		cmbLocSelector.select(selectionValue);
		this.previousSelection = selectionValue;

		btnGetFileElsewhere = new Button(this, SWT.CHECK);
		btnGetFileElsewhere.setEnabled( (delegate.localEnabled && !delegate.remoteEnabled) ? true : false ); 
		btnGetFileElsewhere.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		btnGetFileElsewhere.setText(Messages.getString("TableView.lblToggleBonjourClient")); //$NON-NLS-1$
		btnGetFileElsewhere.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				handleRemoteSelector(((Button) e.widget).getSelection(), true);
			}
		});
		btnGetFileElsewhere.setSelection( delegate.getRemote );

		btnProcessFile = new Button(this, SWT.NONE);
		btnProcessFile.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				handleProcessButton();
			}
		});
		btnProcessFile.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		btnProcessFile.setText(Messages.getString("TableView.btnProcessFile")); //$NON-NLS-1$
		btnProcessFile.setEnabled(delegate.localEnabled);
		
		constructConditionalUI( (delegate.getRemote | delegate.remoteEnabled), delegate.localEnabled );
		
		lblStatusBar = new Label(this, SWT.NONE);
		lblStatusBar.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 3, 1));
		lblStatusBar.setText(Messages.getString("TableView.statusbarFileNotLoaded")); //$NON-NLS-1$
		
		bonjourIcon = new Label(this, SWT.NONE);
		bonjourIcon.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		setBonjourDisabled(null);
		
		handleRemoteSelector( delegate.getRemote, false );
		
		createContents();
	}
	
	private void createContents( ) {
		setImages(delegate.getImages());
		setText(RadarConsts.PROGRAM_NAME);
		setSize(450, 300);

		addDisposeListener(new DisposeListener() {
			@Override
			public void widgetDisposed(DisposeEvent e) {
				delegate.saveBoundsAsPref(RadarConsts.PREF_KEY_TABLE_WINDOW, TableView.this.getBounds());
				delegate.stopUpdateChecker();
			}
		});
	}
	private void constructConditionalUI(boolean showNetwork, boolean showTable) {
		if (conditionalUI != null) {
			ArrayList<Control> childList = new ArrayList<Control>(Arrays.asList(conditionalUI.getChildren()));
			for (Control child : childList) {
				child.dispose();
			}
		} else {
			conditionalUI = new Composite(this, SWT.NONE);
			GridLayout gl_composite = new GridLayout(1, false);
			gl_composite.marginWidth = 1;
			gl_composite.horizontalSpacing = 0;
			conditionalUI.setLayout(gl_composite);
			conditionalUI.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 4, 1));
		}

		if (showNetwork) {
			grpNetworkInfo = new Group(conditionalUI, SWT.NONE);
			grpNetworkInfo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
			grpNetworkInfo.setText(Messages.getString("TableView.lblNetworkGrouping")); //$NON-NLS-1$
			grpNetworkInfo.setLayout(new GridLayout(6, false));

			Label lblHost = new Label(grpNetworkInfo, SWT.NONE);
			lblHost.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, false, 1, 1));
			lblHost.setText(Messages.getString("TableView.lblHost")); //$NON-NLS-1$

			txtHostname = new Text(grpNetworkInfo, SWT.BORDER );
			txtHostname.setEditable(false);
			txtHostname.setText( hostnameText );
			txtHostname.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));

			Label lblPort = new Label(grpNetworkInfo, SWT.NONE);
			lblPort.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, false, 1, 1));
			lblPort.setText(Messages.getString("TableView.lblPort")); //$NON-NLS-1$

			txtPort = new Text(grpNetworkInfo, SWT.BORDER );
			txtPort.setEditable(false);
			txtPort.setText( portText );
			txtPort.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
			
			btnScan = new Button(grpNetworkInfo, SWT.NONE);
			btnScan.setEnabled(false);
			btnScan.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
			btnScan.setText(Messages.getString("TableView.btnFindComputer")); //$NON-NLS-1$
			btnScan.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					TableView.this.delegate.startScanner();
				}
			});
		}
		
		if (showTable) {
			table = new RadarLocalTable(conditionalUI, delegate);
		}
		
		conditionalUI.layout();
	}
	public void resetDisplay( ) {
		if (table != null) table.resetTable();
	}
	
	protected void setBonjourDisabled(Boolean boolObject) {
		if (bonjourIcon == null || (bonjourIcon != null && bonjourIcon.isDisposed())) {
			return;
		}
		
		if (boolObject == null) {
			bonjourIcon.setImage(SWTResourceManager.getImage(TableView.class, "/resources/blank.png")); //$NON-NLS-1$
			bonjourIcon.setToolTipText(Messages.getString("Empty")); //$NON-NLS-1$
		} else if (boolObject.equals(Boolean.FALSE)) {
			bonjourIcon.setImage(SWTResourceManager.getImage(TableView.class, "/resources/Bonjour32.png")); //$NON-NLS-1$
			bonjourIcon.setToolTipText(Messages.getString("TableView.tooltipBonjourEnabled")); //$NON-NLS-1$
			if (delegate.getRemote && btnScan != null && !btnScan.isDisposed()) {
				btnScan.setEnabled(true);
				btnScan.setToolTipText(Messages.getString("Empty")); //$NON-NLS-1$
			}
		} else if (boolObject.equals(Boolean.TRUE)) {
			bonjourIcon.setImage(SWTResourceManager.getImage(TableView.class, "/resources/Bonjour32-NotActive.png")); //$NON-NLS-1$
			bonjourIcon.setToolTipText(Messages.getString("TableView.tooltipBonjourDisabled")); //$NON-NLS-1$

			if (delegate.getRemote && btnScan != null && !btnScan.isDisposed()) {
				btnScan.setEnabled(false);
				btnScan.setToolTipText(Messages.getString("TableView.tooltipScanDisabled")); //$NON-NLS-1$
			}
		}
	}
	
	protected void handleRemoteSelector( boolean selection, boolean redraw ) {
		delegate.getRemote = selection;
		delegate.setPreference(RadarConsts.PREF_KEY_GET_REMOTE, selection);
		
		if (redraw) {
			constructConditionalUI((delegate.getRemote | delegate.remoteEnabled), true);
		}
		
		txtFilename.setEditable(!selection);
		cmbLocSelector.setEnabled(!selection);
		btnSelectFile.setEnabled(!selection);
		
		if (txtHostname != null && txtPort != null && btnScan != null && 
				!txtHostname.isDisposed() && !txtPort.isDisposed() && !btnScan.isDisposed()) {
			txtHostname.setEditable(selection);
			txtPort.setEditable(selection);
		}
		
		Boolean checkBrowser = null;
		if (selection) {
			checkBrowser = new Boolean(!delegate.checkForBrowser());
		}
		setBonjourDisabled(checkBrowser);
		
		conditionalUI.redraw();
	}
	protected void handleProcessButton( ) {
		String filename = txtFilename.getText();

		if (filename.equals(Messages.getString("Empty")) && !delegate.getRemote) { //$NON-NLS-1$
			delegate.displayAlert(Messages.getString("TableView.NoFile")); //$NON-NLS-1$
			return; 
		}
		if (! delegate.getPreference(RadarConsts.PREF_KEY_FILENAME).equals(filename) ) {
			delegate.setPreference(RadarConsts.PREF_KEY_FILENAME, filename);
		}
		
		delegate.processFile();
	}
	protected void handleFileSelectButton( ) {
		FileDialog dialog;
		String filename;

		dialog = new FileDialog(this, SWT.OPEN);
		dialog.setFilterExtensions( new String[] {Messages.getString("TableView.ExtensionCohdemo"), Messages.getString("TableView.ExtensionsWildcard")} ); //$NON-NLS-1$ //$NON-NLS-2$
		dialog.setFilterNames(new String[] {Messages.getString("TableView.ExtensionCohdemoExplanation"), Messages.getString("TableView.ExtensionWildcardExplanation")} ); //$NON-NLS-1$ //$NON-NLS-2$

		File test = new File(txtFilename.getText());
		dialog.setFilterPath(test.getPath());

		dialog.setText(Messages.getString("TableView.dialogFileSelectTitle")); //$NON-NLS-1$

		filename = dialog.open();

		//the file dialog returns a NULL reference if cancel was clicked
		if (filename == null) {
			return;
		}

		delegate.setPreference(RadarConsts.PREF_KEY_FILENAME, filename);

		txtFilename.setText(filename);
		txtFilename.setSelection(filename.length());

		forceFocus();
	}
	protected void handleLocSelector( ) {
		int selectionIndex = cmbLocSelector.getSelectionIndex();
		if ( previousSelection == selectionIndex) {
			return;
		}
		
		delegate.setLocalRemoteFlags(((selectionIndex == 0 || selectionIndex == 2) ? true : false), 
				                     ((selectionIndex == 1 || selectionIndex == 2) ? true : false));
		delegate.generateWindows();
		
		switch (selectionIndex)	 {
			case 0:
				delegate.remoteDisable();
				break;
			case 1:
				delegate.remoteEnable();
				break;
			case 2:
				delegate.remoteEnable();
				break;
			default:
				break;
		}
		previousSelection = selectionIndex;

		if (!isDisposed()) {
			//Layout is rebuilt when switching to or from local-enabled
			conditionalUI.layout(true);
		}
	}
}
