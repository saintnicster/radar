package com.coh.radar;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;

import com.apple.dnssd.DNSSD;
import com.apple.dnssd.DNSSDException;
import com.apple.dnssd.DNSSDRegistration;
import com.apple.dnssd.DNSSDService;
import com.apple.dnssd.RegisterListener;

public class RadarSocketServer implements Runnable, RegisterListener {
	public static String REG_TYPE = Messages.getString("BonjourServiceType"); //$NON-NLS-1$

	DNSSDRegistration registration;
	MainApplication   delegate;
	ServerSocket      server;
	ArrayList<Socket> activeConnections;

	String dnssdServiceName = Messages.getString("Empty"); //$NON-NLS-1$
	String dnssdDomain = Messages.getString("Empty"); //$NON-NLS-1$

	private InetSocketAddress addressInfo;

	private boolean serverIsRunning = false;
	private boolean noBonjour = false;

	public RadarSocketServer(MainApplication delegate) throws RadarException {
		this.delegate = delegate;

		this.createServer();

		activeConnections = new ArrayList<Socket>();
	}
	public String getServiceName() {
		return ((serverIsRunning) ? dnssdServiceName : Messages.getString("Empty")); //$NON-NLS-1$
	}
	public boolean isServerRunning() {
		return serverIsRunning;
	}
	public void createServer( ) throws RadarException {
		if (server == null || !server.isBound() || server.isClosed()) {
			try {
				server = new ServerSocket(0);
				setAddressInfo( new InetSocketAddress( InetAddress.getLocalHost(), 
						                               server.getLocalPort()) );
			} catch (IOException e) {
				e.printStackTrace();
				throw new RadarException(Messages.getString("RadarSocketServer.ErrorServerCreate"), e); //$NON-NLS-1$
			}
		}
	}

	public Thread start() throws RadarException {
		Thread serverThread = new Thread(this);
		try {
			serverThread.start();
		} catch (RuntimeException e) {
			e.printStackTrace();
			throw new RadarException(Messages.getString("RadarSocketServer.ErrorServerStart"), e); //$NON-NLS-1$
		}
		return serverThread;
	}

	public void stop() throws RadarException {
		try {
			server.close();
		} catch (IOException e) {
			e.printStackTrace();
			throw new RadarException(Messages.getString("RadarSocketServer.ErrorServerClose"), e); //$NON-NLS-1$
		}
	}

	@Override
	public void run() {
		try {
			if (serverIsRunning) {
				throw new RuntimeException(Messages.getString("RadarSocketServer.ErrorServerAlreadyRunning"), null); //$NON-NLS-1$
			}

			serverIsRunning = true;
			try {
				//dnssdServiceName is intentionally made blank.  
				//  The framework will automatically create/populate a name for you if left blank 
				dnssdServiceName = Messages.getString("Empty"); //$NON-NLS-1$
				registration = DNSSD.register(dnssdServiceName, RadarSocketServer.REG_TYPE, (getAddressInfo().getPort()), this);
			} catch (NoClassDefFoundError e) {
				e.printStackTrace();
				noBonjour = true;
				
				acceptConnections();
			} catch (UnsatisfiedLinkError e) {
				e.printStackTrace();
				noBonjour = true;
				
				acceptConnections();
			} catch (DNSSDException e) {
				e.printStackTrace();
				throw new RuntimeException(Messages.getString("RadarSocketServer.ErrorBonjour"), e); //$NON-NLS-1$
			}
		} catch (RuntimeException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void operationFailed(DNSSDService service, int errorCode) {
		System.out.println(Messages.getString("RadarSocketServer.ServiceInformation")+service); //$NON-NLS-1$
		System.out.println(Messages.getString("ErrorCode")+errorCode); //$NON-NLS-1$
		noBonjour = true;
		
		acceptConnections();
	}

	@Override
	public void serviceRegistered(DNSSDRegistration registration, int flags, 
			String serviceName, String regType, String domain) {
		this.dnssdServiceName = serviceName;
		this.dnssdDomain = domain;

		acceptConnections();
	}

	public int pushFileToConnections() throws RadarException {
		int counter = 0;
		if (!activeConnections.isEmpty()) {
			ArrayList<Socket> tempConnections = new ArrayList<Socket>();
			for (Socket socket : activeConnections) {
				if (socket.isBound() && !socket.isClosed()) {
					
					if (delegate.usePListFormatting) {
						RadarSocketServerThreadPlist socketThread = new RadarSocketServerThreadPlist(socket, delegate.cdp.entityMap, delegate.getFileToServe());
						socketThread.serveFile();
					} else {
						RadarSocketServerThread socketThread = new RadarSocketServerThread(socket, delegate.getFileToServe());
						socketThread.serveFile();
					}
					counter++;
					tempConnections.add(socket);
				}
			}
			activeConnections = tempConnections;
		}
		return counter;
	}

	public void acceptConnections() {
		delegate.updateServerUI();
		
		while (serverIsRunning) {
			try {
				createServer();

				Socket connection = server.accept();
				activeConnections.add(connection);
				
				if (delegate.usePListFormatting) {
					delegate.processFile();
				} else {
					RadarSocketServerThread socketThread = new RadarSocketServerThread(connection, delegate.getFileToServe());
					socketThread.serveFile();
				}
			} catch (RadarException e) {
				e.printStackTrace();
			} catch (SocketException e) {
				//Triggered when the server is closed while waiting to accept a new connection
				serverIsRunning = false;
			} catch (IOException e){
				e.printStackTrace();
				serverIsRunning = false;
			}
		}

		if (server != null && !server.isClosed()) {
			try {
				server.close();
			} catch (IOException e) {
				e.printStackTrace();
				throw new RuntimeException(Messages.getString("RadarSocketServer.ErrorServerClose"), e); //$NON-NLS-1$
			}
		}

		if ( !this.dnssdServiceName.equals(Messages.getString("Empty")) || registration != null ) { //$NON-NLS-1$
			registration.stop();
		}
	}
	/**
	 * @param inetAddress the inetAddress to set
	 */
	public void setAddressInfo(InetSocketAddress addressInfo) {
		this.addressInfo = addressInfo;
	}
	/**
	 * @return the inetAddress
	 */
	public InetSocketAddress getAddressInfo() {
		return addressInfo;
	}
	public boolean bonjourIsDisabled() {
		return noBonjour;
	}
}
