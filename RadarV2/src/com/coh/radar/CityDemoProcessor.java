package com.coh.radar;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.StringTokenizer;

import org.eclipse.swt.graphics.Point;

import com.coh.radar.Entity.EntityType;

public class CityDemoProcessor {

	public static final String 
	CMD_YOU         = "Player", //$NON-NLS-1$
	CMD_TEAMMATE    = "COSTUME", //$NON-NLS-1$
	CMD_MODEL       = "NPC", //$NON-NLS-1$
	CMD_POSITION    = "POS",  //$NON-NLS-1$
	CMD_CREATEOBJ   = "NEW",  //$NON-NLS-1$
	CMD_DESTROY     = "DEL", //$NON-NLS-1$
	CMD_EFFECT      = "FX", //$NON-NLS-1$
	CMD_HITPOINTS   = "HP", //$NON-NLS-1$
	CMD_ANIMATION   = "MOV", //$NON-NLS-1$
	CMD_RAGDOLL     = "EntRagdoll",//$NON-NLS-1$
	OBJ_CAMERA      = "CAM", //$NON-NLS-1$
	OBJ_SKY         = "SKYFILE"; //$NON-NLS-1$

	public static final String 
	FX_GLOWIE                = "GENERIC/MISSIONOBJECTIVE.FX", //$NON-NLS-1$
	FX_DESRUCT_OBJ           = "GENERIC/IMDESTRUCTIBLE/IMDESTRUCTIBLE.FX", //$NON-NLS-1$
	FX_TRANSPARENT           = "OBJECTIVEOBJECTFX/GHOSTED.FX", //$NON-NLS-1$
	FX_TRANSPARENT2          = "OBJECTIVEOBJECTFX/GHOSTEDNOGLOWIE.FX", //$NON-NLS-1$
	FX_NICTIGUN              = "WEAPONS/NICTUSHUNTERRIFLE.FX", //$NON-NLS-1$
	FX_CONTACT_HAS_MISSION   = "UI/ICON_MISSIONAVAILABLE.FX", //$NON-NLS-1$
	FX_CONTACT_MISSON_ACTIVE = "UI/ICON_MISSIONINPROGRESS.FX", //$NON-NLS-1$
	FX_CONTACT_COMPLETED     = "UI/ICON_MISSIONCOMPLETED.FX", //$NON-NLS-1$
	MOV_HOSTAGE              = "FEARSTUN", //$NON-NLS-1$
	MOV_CAPTURE              = "SPELLCASTFEAR", //$NON-NLS-1$
	MOV_CAPTURE_2            = "CAPTURED_PENELOPE_CYCLE", //$NON-NLS-1$
	MOV_OUTCOLD_3            = "OUTCOLD03", //$NON-NLS-1$
	NPC_CYST                 = "Shadow_Crystal", //NPC command //$NON-NLS-1$
	NPC_NICTUS               = "Warshade_Extraction", //Unbound Nictus, Greater unbound nictus //$NON-NLS-1$
	NPC_VOID                 = "Nictus_Hunter", //$NON-NLS-1$
	NPC_DWARF                = "Kheldian_NPC_Dwarf", //$NON-NLS-1$
	NPC_NOVA                 = "Kheldian_NPC_Nova", //$NON-NLS-1$
	NAME_QUANTUM             = "Quantum", //$NON-NLS-1$
	HP_DEAD                  = "0.00"; //$NON-NLS-1$

	protected EntityMap entityMap;
	protected String filename = Messages.getString("Empty"); //$NON-NLS-1$

	ArrayList<String> fileContents = null;

	//Initialize these at the polar extreme for proper comparisons
	private int minX = Integer.MAX_VALUE, maxX = Integer.MIN_VALUE,
	            minY = Integer.MAX_VALUE, maxY = Integer.MIN_VALUE;

	CityDemoProcessor(String impFilename) {
		this.filename = impFilename;

		this.entityMap = new EntityMap();
	}
	public Point getMaxBound() {
		return new Point(maxX, maxY);
	}
	public Point getMinBound() {
		return new Point(minX, minY);
	}
	
	public void setMinMaxBounds(float xPos, float yPos) {

		if (xPos > maxX) maxX = (int) xPos;
		if (xPos < minX) minX = (int) xPos;
		if (yPos > maxY) maxY = (int) yPos;
		if (yPos < minY) minY = (int) yPos;
		
	}
	
	public void processFile() throws RadarException
	{
		if (this.filename.indexOf(Messages.getString("PlistFileExtension")) > 0) { //$NON-NLS-1$
			entityMap = EntityMap.buildMapFromPList(this.filename);
			
			for (Entity tmpEntity : entityMap.getEntityList()) {
				setMinMaxBounds(tmpEntity.posXAxis, tmpEntity.posYAxis);
			}
			return;
		}
		
		
		BufferedReader br = null;
		FileReader fr = null;

		StringTokenizer st = null;
		Entity entity = null;
		String demoLine = Messages.getString("Empty"); //$NON-NLS-1$
		String objID, cmd, entityName;

		int tokenCount = Integer.MIN_VALUE;
		float posXAxis = 0.0f, posYAxis = 0.0f, posZAxis = 0.0f;

		fileContents = new ArrayList<String>();

		try {
			fr = new FileReader(this.filename);
			br = new BufferedReader(fr);
		} catch (FileNotFoundException e) {
			throw new RadarException("Unable to read file: "+ this.filename, e); //$NON-NLS-1$
		}
		try {
			while( ( demoLine = br.readLine( )) != null ) {
				fileContents.add(demoLine);

				st = new StringTokenizer(demoLine);
				tokenCount = st.countTokens();

				if (tokenCount < 3) {
					System.out.println("Malformed Line: "+ demoLine); //$NON-NLS-1$
					continue;
				}
				st.nextToken(); //This is the timing code, not used by the Radar
				objID = st.nextToken(); //The ObjectID is the second token
				cmd	  = st.nextToken(); //Demo Command

				try { //any object IDs that we care about will be numbers.  use this to filter them out
					Integer.parseInt(objID);
				} catch (NumberFormatException e) {
					continue;
				}
				entity = entityMap.getEntity( objID );
				if ( entity.entityName.equals("Dr") && entity.entityType == EntityType.NONE ) { //$NON-NLS-1$
					entity.setEntityType(EntityType.DOOR);
				}
				if ( cmd.equals(CMD_CREATEOBJ) ) {
					entityName = parseEntityName(st);

					entity.setEntityName(entityName);
				} else if ( cmd.equals(CMD_POSITION) ) {
					if (tokenCount < 6) { 
						System.out.println("Bad Coordinates: "+demoLine); //$NON-NLS-1$
						continue;
					}

					try {
						posXAxis = Float.parseFloat(st.nextToken());
						posZAxis = Float.parseFloat(st.nextToken());
						posYAxis = Float.parseFloat(st.nextToken());
						
						setMinMaxBounds( posXAxis, posYAxis );
						
						entity.setCoordinates(posXAxis, posZAxis, posYAxis);
					} catch (NumberFormatException e) {
						e.printStackTrace();
					}
				} else if ( cmd.equals( CMD_YOU ) ) {
					entity.setEntityType( EntityType.PLAYER );
					entityMap.setPlayerObjectID(objID);
				} else if (cmd.equals( CMD_EFFECT )) {
					//Read the attributes, looking for possible glowies
					if (demoLine.indexOf(FX_GLOWIE) != -1) { //String wasn't found it is -1
						entity.setEntityType(EntityType.GLOWIE);
					} else if (demoLine.indexOf(FX_TRANSPARENT) != -1 ) {
						entity.setEntityType(EntityType.ANTIGLOWIE);
					} else if (demoLine.indexOf(FX_TRANSPARENT2) != -1) {
						entity.setEntityType(EntityType.ANTIGLOWIE);
					} else if (demoLine.indexOf(FX_NICTIGUN) != -1) {
						entity.setEntityType(EntityType.QUANTUM);
					} else if (demoLine.indexOf(FX_DESRUCT_OBJ) != -1) {
						entity.setEntityType(EntityType.DESTRUCT);
					} else if (demoLine.indexOf(FX_CONTACT_HAS_MISSION) != -1) {
						entity.setEntityType(EntityType.CONTACT_HAS_MISH);
					} else if (demoLine.indexOf(FX_CONTACT_MISSON_ACTIVE) != -1) {
						entity.setEntityType(EntityType.CONTACT_INPROG);
					} else if (demoLine.indexOf(FX_CONTACT_COMPLETED) != -1) {
						entity.setEntityType(EntityType.CONTACT_MISSIONOVER);
					}
				} else if (cmd.equals( CMD_HITPOINTS )) {
					if (tokenCount < 4) {
						System.out.println("Bad Hitpoints Line: "+demoLine); //$NON-NLS-1$
						continue;
					}
					if (st.nextToken().equals( HP_DEAD )) {
						entity.makeDead();
					}
				} else if (cmd.equals(CMD_TEAMMATE)) {
					if (entity.entityType == EntityType.NONE) {
						entity.setEntityType(EntityType.TEAMMATE);
					}
				} else if (cmd.equals(CMD_ANIMATION)) {
					if (tokenCount < 4) {
						System.out.println("Bad Animations Line: "+demoLine); //$NON-NLS-1$
						continue;
					}
					if (demoLine.indexOf(MOV_HOSTAGE) != -1) {
						entity.setEntityType(EntityType.HOSTAGE);
					} else if (demoLine.indexOf(MOV_CAPTURE) != -1) {
						entity.setEntityType(EntityType.CAPTIVE);
					} else if (demoLine.indexOf(MOV_CAPTURE_2) != -1) {
						entity.setEntityType(EntityType.CAPTIVE);
					}	
				} else if (cmd.equals(CMD_MODEL)) {
					if (demoLine.indexOf(NPC_CYST) != -1) {
						entity.setEntityType(EntityType.QUANTUM);
					} else if (demoLine.indexOf(NPC_NICTUS) != -1) {
						entity.setEntityType(EntityType.QUANTUM);
					} else if (demoLine.indexOf(NPC_VOID) != -1) {
						entity.setEntityType(EntityType.QUANTUM);
					} else if (demoLine.indexOf(NPC_DWARF) != -1) {
						entity.setEntityType(EntityType.QUANTUM);
					} else if (demoLine.indexOf(NPC_NOVA) != -1) {
						entity.setEntityType(EntityType.QUANTUM);
					}
				}
			}
		} catch (IOException e) {
			throw new RadarException("Error reading line ", e); //$NON-NLS-1$
		} finally {
			if (fr != null) { try { fr.close(); } catch (IOException e) {e.printStackTrace();} }
			if (br != null) { try { br.close(); } catch (IOException e) {e.printStackTrace();} }
		}
	}
	private String parseEntityName( StringTokenizer st) {
		String output = Messages.getString("Empty"); //initialize Exported line //$NON-NLS-1$

		while( st.hasMoreTokens() ) {
			output = output.concat(" " + st.nextToken()); //$NON-NLS-1$
		}

		if ( output.startsWith(" \"") && output.endsWith("\"") ) { //$NON-NLS-1$ //$NON-NLS-2$
			output = output.substring(2, (output.length() - 1) );
		}

		output = output.trim();	
		return output;
	}

	public ArrayList<String> getFileContents() {
		return fileContents;
	}
}
