package com.coh.radar;

public class Version {
	private static String SEPERATOR = ".";//$NON-NLS-1$
	private static int DEFAULT_VALUE = Integer.MIN_VALUE;
	public int major, minor, build, revision;
	protected int compareInt(int i1, int i2) {
		Integer int1 = new Integer(i1), int2 = new Integer(i2);
		return int1.compareTo(int2);
	}
	public int compareTo(Version anotherVersion) {
		int returnValue = 0;
		
		returnValue = compareInt(this.major, anotherVersion.major);
		if (returnValue == 0) {
			returnValue = compareInt(this.minor, anotherVersion.minor);
			if (returnValue == 0) {
				returnValue = compareInt(this.build, anotherVersion.build);
				if (returnValue == 0) {
					returnValue = compareInt(this.revision, anotherVersion.revision);
				}
			}
		}
		
		return returnValue;
	}
	public boolean equals(Version anotherVersion) {
		boolean retValue = false;
		
		if (this.major == anotherVersion.major && 
				this.minor == anotherVersion.minor && 
				this.build == anotherVersion.build && 
				this.revision == anotherVersion.revision) {
			retValue = true;
		}
		
		return retValue;
	}
	public Version(int major, int minor, int build, int revision) {
		this.major = major;
		this.minor = minor;
		this.build = build;
		this.revision = revision;
	}
	public static Version VersionFromString(String versionString) {
		String[] parts = null;
		Version retVal = null;
		if (versionString.indexOf(Version.SEPERATOR) != -1) {
			parts = versionString.split("\\"+Version.SEPERATOR); //$NON-NLS-1$
		}
		
		if (parts == null || parts.length == 0) {
			retVal = new Version();
		} else {
			int major = Version.DEFAULT_VALUE, minor = Version.DEFAULT_VALUE, build = Version.DEFAULT_VALUE, revision = Version.DEFAULT_VALUE;
			try {
				major = Integer.parseInt(parts[0]);
				minor = Integer.parseInt(parts[1]);
				build = Integer.parseInt(parts[2]);
				revision = Integer.parseInt(parts[3]);
			} catch (NumberFormatException nfe) {
			}
			
			retVal = new Version(major, minor, build, revision);
		}
		return retVal;
	}
	public Version() {
		this(Version.DEFAULT_VALUE,Version.DEFAULT_VALUE,Version.DEFAULT_VALUE,Version.DEFAULT_VALUE);
	}
	public Version(int major) {
		this(major,Version.DEFAULT_VALUE,Version.DEFAULT_VALUE,Version.DEFAULT_VALUE);
	}
	public Version(int major, int minor) {
		this(major,minor,Version.DEFAULT_VALUE,Version.DEFAULT_VALUE);
	}
	public Version (int major, int minor, int build) {
		this(major,minor,build,Version.DEFAULT_VALUE);
	}
	
	public String toString() {
		String retVal = Messages.getString("Empty"); //$NON-NLS-1$
		if (this.major == Version.DEFAULT_VALUE) {
			retVal = "Uninitialized Version Number"; //$NON-NLS-1$
		} else {
			retVal = retVal.concat(Messages.getString("Empty")+this.major); //$NON-NLS-1$
			if (this.minor != Version.DEFAULT_VALUE) retVal = retVal.concat(Version.SEPERATOR+this.minor);
			if (this.build != Version.DEFAULT_VALUE) retVal = retVal.concat(Version.SEPERATOR+this.build);
			if (this.revision != Version.DEFAULT_VALUE) retVal = retVal.concat(Version.SEPERATOR+this.revision);
		}
		
		return retVal;
	}
}
