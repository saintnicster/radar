package com.coh.radar;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.Socket;

public class RadarSocketServerThreadPlist implements Runnable {
	public EntityMap map = null;
	private Socket socket;
	private String filename = "cohdemo"; //$NON-NLS-1$
	
	public RadarSocketServerThreadPlist(Socket socket, EntityMap map, String originalFilename) {
		this.socket = socket;
		this.map = map;
		this.filename = originalFilename;
	}
	
	public Thread serveFile() throws RadarException {
		Thread newThread = new Thread(this);
		newThread.start();
		
		return newThread;
	}

	/* Example code for serving the file over the socket was found at 
	 * 
	 *	http://edinkysoft.appspot.com/jsp/articles/javafilesocket.html
     */
	@Override
	public void run() {
		if (!socket.isBound() || socket.isClosed()) {
			return;
		}
		
		byte[] fileContents = null;
		try {
			fileContents = map.getPListXML().getBytes(Messages.getString("UTF8Charset")); //$NON-NLS-1$
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		File fileHandler = new File(this.filename);
		this.filename = fileHandler.getName();
		this.filename = this.filename.substring(0, this.filename.indexOf(".")); //$NON-NLS-1$
		if (this.filename.length() > 20) {
			this.filename = this.filename.substring(0, 20);
		}
		
		String fileInfo = this.filename+Messages.getString("PlistFileExtension")+Messages.getString("MessageFileInfoSeperator")+fileContents.length; //$NON-NLS-1$ //$NON-NLS-2$
		try {
			byte[] headerBytes = fileInfo.getBytes(Messages.getString("UTF8Charset")); //$NON-NLS-1$ 
			socket.getOutputStream().write(headerBytes, 0, headerBytes.length);
			socket.getOutputStream().flush();
			headerBytes = null; fileInfo = null;
			Thread.sleep(100);
			
			socket.getOutputStream().write(fileContents, 0, fileContents.length);
			socket.getOutputStream().flush();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			try { socket.getOutputStream().close(); } catch (IOException e) { e.printStackTrace(); }
		}
	}
}
