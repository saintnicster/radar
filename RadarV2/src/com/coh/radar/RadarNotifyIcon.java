package com.coh.radar;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Tray;
import org.eclipse.swt.widgets.TrayItem;

public class RadarNotifyIcon extends TrayItem {
	MainApplication delegate;
	@Override
	public void checkSubclass() {
		//leave initial so that we can bundle everything together like this
	}
	
	public RadarNotifyIcon(Tray tray, MainApplication delegate) {
		super(tray, SWT.None);
		
		this.delegate = delegate;
	}
	
	public void makeTrayIcon(Image icon) {
		if (icon != null) {
			setImage(icon);
		}
		setToolTipText(RadarConsts.PROGRAM_NAME);
		
		addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				delegate.bringToForeground();
			}
		});
		addListener(SWT.MenuDetect, new Listener() {
			@Override
			public void handleEvent(Event event) {
				buildMenu( new Shell(event.display) );
			}
		});
	}
	
	protected void buildMenu( Shell parent ) {
		MenuItem menuItem;
		Menu trayMenu = new Menu(parent, SWT.POP_UP);
		
		menuItem = new MenuItem(trayMenu, SWT.PUSH);
		menuItem.setText(Messages.getString("RadarNotifyIcon.ResetWindow")); //$NON-NLS-1$
		menuItem.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				delegate.initializeWindowPosition( );
			}
		});
		menuItem = new MenuItem(trayMenu, SWT.PUSH);
		menuItem.setText(Messages.getString("RadarNotifyIcon.RefreshMap")); //$NON-NLS-1$
		menuItem.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				delegate.processFile();
			}
		});
		menuItem = new MenuItem(trayMenu, SWT.PUSH);
		menuItem.setText(Messages.getString("RadarNotifyIcon.ExitProgram")); //$NON-NLS-1$
		menuItem.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				delegate.close();
			}
		});
		trayMenu.setVisible(true);
	}
	
}
