package com.coh.radar;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.Socket;

public class RadarSocketServerThread implements Runnable {
	
	private Socket socket;
	private String fileToServe;
	
	File fileToSend = null;
	
	BufferedInputStream bis = null;
	FileInputStream fis = null;
	
	public RadarSocketServerThread(Socket socket, String filename) {
		this.socket = socket;
		this.fileToServe = filename;
	}
	
	public Thread serveFile() throws RadarException {
		
		fileToSend = new File(fileToServe);
		
		try {
			fis = new FileInputStream(fileToServe);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RadarException(Messages.getString("RadarSocketServerThread.ErrorBadFile"), e); //$NON-NLS-1$
		} catch (SecurityException e) {
			e.printStackTrace();
			throw new RadarException(Messages.getString("RadarSocketServerThread.ErrorFilePermissions"), e); //$NON-NLS-1$
		}
		if (this.socket == null || !this.socket.isBound() || this.socket.isClosed()) {
			throw new RadarException(Messages.getString("RadarSocketServerThread.ErrorBadSocket"), null); //$NON-NLS-1$
		}
		
		Thread newThread = new Thread(this);
		newThread.start();
		
		return newThread;
	}

	/* Example code for serving the file over the socket was found at 
	 * 
	 *	http://edinkysoft.appspot.com/jsp/articles/javafilesocket.html
     */
	@Override
	public void run() {
		if (!socket.isBound() || socket.isClosed()) {
			return;
		}
		
		byte[] fileContents = new byte[(int) fileToSend.length()];
		
		String fileInfo = fileToSend.getName()+Messages.getString("MessageFileInfoSeperator")+fileToSend.length(); //$NON-NLS-1$
		
		try {
			byte[] headerBytes = fileInfo.getBytes(Messages.getString("UTF8Charset")); //$NON-NLS-1$ 
			socket.getOutputStream().write(headerBytes, 0, headerBytes.length);
			socket.getOutputStream().flush();
			Thread.sleep(200);
			
			bis = new BufferedInputStream(fis);
			bis.read(fileContents, 0, fileContents.length);
			
			socket.getOutputStream().write(fileContents, 0, fileContents.length);
			socket.getOutputStream().flush();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			if (fis != null) { try { fis.close(); } catch (IOException e) { e.printStackTrace(); } }
			if (bis != null) { try { bis.close(); } catch (IOException e) { e.printStackTrace(); } }
			try { socket.getOutputStream().close(); } catch (IOException e) { e.printStackTrace(); }
		}
	}
}
