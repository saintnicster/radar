package com.coh.radar;

public class RadarException extends Exception {
	public RadarException(String string, Exception e) {
		super(string, e);
	}

	private static final long serialVersionUID = -3917935201777725453L;
}
